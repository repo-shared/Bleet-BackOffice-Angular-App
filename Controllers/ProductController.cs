using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Azure.Cosmos;
using Bleet_Web.DBModel;
using Bleet_Web.DBModel.Request;
using System.Net;

namespace Bleet_Web.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly string containerId = "products";

        private readonly ILogger<ProductController> _logger;

        public ProductController(ILogger<ProductController> logger)
        {
            _logger = logger;

        }


        [HttpGet("[action]")]
        public async Task<IActionResult> AllProducts()
        {
            try
            {
                var sqlQueryText = "SELECT * FROM c";

                var list = await CosmosDbContext.GetData<Products>(containerId, sqlQueryText);

                return Ok(list);

            }
            catch (Exception ex)
            {
                return BadRequest(ex);

            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Add(Products data)
        {
            try
            {
                if (data.Id == "0" || data.Id == "")
                    data.Id = DateTime.Now.ToString("yyyyMMddHHmmss");

                if (!String.IsNullOrEmpty(data.Imagen))
                {
                    var resp = await BlobStorageContext.UploadImageFile(data.Id, data.Imagen);
                    if (resp)
                    {
                        data.Imagen = "https://bleetstorage.blob.core.windows.net/bleet-images/" + data.Id + ".png";
                    }

                }

                await CosmosDbContext.AddData<Products>(containerId, data, data.Id);

                return Ok(data);

            }
            catch (Exception ex)
            {
                return BadRequest(ex);

            }
        }
    }

}