using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Bleet_Web.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class SampleController : ControllerBase
    {
       private readonly ILogger<SampleController> _logger;

        public SampleController(ILogger<SampleController> logger)
        {
            _logger = logger;
        }

        
        [HttpGet("[action]")]
        public IActionResult AllSamples()
        {
            var rng = new Random();
            return Ok("Hi All Samples");
        }
    }

}