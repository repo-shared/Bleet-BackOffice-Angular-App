using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Azure.Cosmos;
using Bleet_Web.DBModel;
using Bleet_Web.DBModel.Request;
using System.Net;

namespace Bleet_Web.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly string containerId = "clients";

        private readonly ILogger<ClientController> _logger;

        public ClientController(ILogger<ClientController> logger)
        {
            _logger = logger;

        }

        [HttpGet("[action]")]
        public async Task<IActionResult> AllClients()
        {
            try
            {
                var sqlQueryText = "SELECT * FROM c";
                
                var list = await CosmosDbContext.GetData<Client>(containerId, sqlQueryText);
                
                return Ok(list);
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex);

            }
        }

         [HttpGet("[action]")]
        public async Task<IActionResult> GetById(string id)
        {
            try
            {
                var sqlQueryText = "SELECT * FROM c where id = " + id;
                
                var list = await CosmosDbContext.GetData<Client>(containerId, sqlQueryText);
                
                return Ok(list);
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex);

            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Add(Client data)
        {
            try
            {
                await CosmosDbContext.AddData<Client>(containerId, data, data.Id);
   
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    
    }

}