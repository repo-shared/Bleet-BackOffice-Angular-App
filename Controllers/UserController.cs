using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Azure.Cosmos;
using Bleet_Web.DBModel;
using Bleet_Web.DBModel.Request;
using System.Net;

namespace Bleet_Web.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly string containerId = "users";

        private readonly ILogger<UserController> _logger;

        public UserController(ILogger<UserController> logger)
        {
            _logger = logger;

        }


        [HttpGet("[action]")]
        public async Task<IActionResult> AllUsers()
        {
            try
            {
                var sqlQueryText = "SELECT * FROM c";
                
                var usr = new Users
                {
                    Id = "1",
                    User = "adminbleet",
                    Pass = "adminbleet123450"
                };

                await CosmosDbContext.AddData<Users>(containerId, usr, usr.Id);

                var list = await CosmosDbContext.GetData<Users>(containerId, sqlQueryText);
                
                return Ok(list);
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex);

            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Login(Login data)
        {
            try
            {
                 var sqlQueryText = "SELECT * FROM c WHERE c.User = '" + data.UserName + "' And c.Pass = '" + data.Password + "'";
                
                
                var usr = new Users
                {
                    Id = "1",
                    User = "adminbleet",
                    Pass = "adminbleet123450"
                };

                await CosmosDbContext.AddData<Users>(containerId, usr, usr.Id);

                var list = await CosmosDbContext.GetData<Users>(containerId, sqlQueryText);
                
                return Ok(list);
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex);

            }
        }
    }

}