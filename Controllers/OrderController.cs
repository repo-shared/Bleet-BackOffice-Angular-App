using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Azure.Cosmos;
using Bleet_Web.DBModel;
using Bleet_Web.DBModel.Request;
using System.Net;

namespace Bleet_Web.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly string containerId = "orders";

        private readonly ILogger<OrderController> _logger;

        public OrderController(ILogger<OrderController> logger)
        {
            _logger = logger;

        }

        [HttpGet("[action]")]
        public async Task<IActionResult> AllOrders()
        {
            try
            {
                var sqlQueryText = "SELECT * FROM c";
                
                var list = await CosmosDbContext.GetData<Orders>(containerId, sqlQueryText);
                
                return Ok(list);
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex);

            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Add(Orders data)
        {
            try
            {
                await CosmosDbContext.AddData<Orders>(containerId, data, data.Id);
   
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    
    }

}