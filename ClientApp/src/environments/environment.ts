// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    //WebApiUrl: "https://overlordteam-express-api.azurewebsites.net/",
    WebApiUrl: "https://localhost:5001/",
    firebase: {
        apiKey: "AIzaSyD7hz8w22frGqHwU2JIq-rZcG6cu-dW7Hk",
        authDomain: "ocexpress-d721a.firebaseapp.com",
        databaseURL: "https://ocexpress-d721a.firebaseio.com",
        projectId: "ocexpress-d721a",
        storageBucket: "ocexpress-d721a.appspot.com",
        messagingSenderId: "581482755433",
        appId: "1:581482755433:web:5cf62115e68c524b"
    }
};
