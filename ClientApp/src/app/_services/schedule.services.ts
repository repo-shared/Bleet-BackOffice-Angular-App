import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpOptions } from './http.options';
import { Observable } from 'rxjs';

@Injectable()
export class SchedulesService {

    constructor(private http: Http) { }

    getSchedules() {
        return this.http.get(HttpOptions.getWebApiUrl() + '/api/general/WorkSchedule', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }
    insert(item) {
        return this.http.post(HttpOptions.getWebApiUrl() + 'api/general/WorkSchedule', { data: [item] },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    update(item) {
        return this.http.put(HttpOptions.getWebApiUrl() + 'api/general/WorkSchedule', { data: [item] },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }
}
