import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpOptions } from './http.options';
import { DatePipe } from '@angular/common';

@Injectable()
export class ProductService {
    private dp = new DatePipe("en-US");

    constructor(private http: HttpClient) { }

    getAllProducts(): Observable<Products> {
        return this.http.get<Products>(HttpOptions.getWebApiUrl() + 'api/product/AllProducts', HttpOptions.getRequestOptionsHttpClient());
    }

    add(item) {
        return this.http.post(HttpOptions.getWebApiUrl() + 'api/product/Add', item, HttpOptions.getRequestOptionsHttpClient());
    }

   
}