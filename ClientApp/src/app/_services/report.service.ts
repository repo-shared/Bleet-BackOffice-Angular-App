import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpOptions } from './http.options';
import { Observable, Subject } from 'rxjs';
import { DatePipe } from '@angular/common';

@Injectable()
export class ReportService {
    private dp = new DatePipe("en-US");

    constructor(private http: Http) {
    }

    getMilestone(dateIni, dateEnd, userId, subsidiaryId) {
        var urlMethod = 'api/reports/RptPackage/GetMilestone' +
            '?date1=' + encodeURIComponent(this.dp.transform(dateIni, 'yyyy-MM-dd') + ' 00:00:00') +
            '&date2=' + encodeURIComponent(this.dp.transform(dateEnd, 'yyyy-MM-dd') + ' 23:30:00');

        if (userId != '0') {
            urlMethod += '&userId=' + userId;
        }

        if (subsidiaryId != '0') {
            urlMethod += '&subsidiaryId=' + subsidiaryId;
        }

        return this.http.get(HttpOptions.getWebApiUrl() + urlMethod,
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    getActivityLogs(dateIni, dateEnd, userId, subsidiaryId) {

        var urlMethod = 'api/reports/RptUser/GetActivityLogs' +
            '?date1=' + encodeURIComponent(this.dp.transform(dateIni, 'yyyy-MM-dd') + ' 00:00:00') +
            '&date2=' + encodeURIComponent(this.dp.transform(dateEnd, 'yyyy-MM-dd') + ' 23:30:00');

        if (userId != '0') {
            urlMethod += '&userId=' + userId;
        }

        if (subsidiaryId != '0') {
            urlMethod += '&subsidiaryId=' + subsidiaryId;
        }

        return this.http.get(HttpOptions.getWebApiUrl() + urlMethod,
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

}
