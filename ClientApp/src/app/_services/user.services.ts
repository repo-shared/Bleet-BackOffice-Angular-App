import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpOptions } from './http.options';
import { DatePipe } from '@angular/common';

@Injectable()
export class UserService {
    private dp = new DatePipe("en-US");

    constructor(private httpold: Http, private http: HttpClient) { }

    Login(userLogin): Observable<Users[]> {
        return this.http.post<Users[]>(HttpOptions.getWebApiUrl() + 'api/user/Login', userLogin, HttpOptions.getRequestOptionsHttpClient());
    }

    getAll() {
        return this.httpold.get(HttpOptions.getWebApiUrl() + 'api/user/allusers',
        HttpOptions.getRequestOptions()).map((res: Response) => res.json());
        //return this.http.get<Users[]>(HttpOptions.getWebApiUrl() + 'api/user/allusers', HttpOptions.getRequestOptionsHttpClient());
    }

    update(item) {
        return this.httpold.put(HttpOptions.getWebApiUrl() + 'api/users/user', item, HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    getAllBySubsidiary(idSubsidiary) {
        return this.httpold.get(HttpOptions.getWebApiUrl() + 'api/users/user?idSubsidiary=' + idSubsidiary, HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    deleteWorkScheduleUser(id) {
        var http = HttpOptions.getRequestOptions();
        http.body = id;
        return this.httpold.delete(HttpOptions.getWebApiUrl() + 'api/general/WorkScheduleUser', http).map((res: Response) => res.json());

    }

    getWorkDayHours() {

        return this.httpold.get(HttpOptions.getWebApiUrl() + 'api/general/WorkDayHour', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    getWorkDayHoursId(idsche) {

        return this.httpold.get(HttpOptions.getWebApiUrl() + 'api/general/WorkDayHour?IdWorkSchedule=' + idsche, HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }


    getWorkSchedules() {
        return this.httpold.get(HttpOptions.getWebApiUrl() + 'api/general/WorkSchedule', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    getByIdUserWorkSchedule(idUser) {
        return this.httpold.get(HttpOptions.getWebApiUrl() + 'api/general/WorkScheduleUser/GetByIdUser?idUser=' + idUser, HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    postWorkScheduleUser(userWorkSchedule) {
        return this.httpold.post(HttpOptions.getWebApiUrl() + 'api/general/WorkScheduleUser', { data: userWorkSchedule }, HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }


    getGenders() {

        let gender: { id: number, name: string }[] = [
            { "id": 0, "name": "F" },
            { "id": 1, "name": "M" }
        ];

        return gender;
    }

    getActivityLogs(dateIni, dateEnd, userId, subsidiaryId) {
        var urlMethod = 'api/users/UserStatus/GetActivityLogs' +
            '?date1=' + encodeURIComponent(this.dp.transform(dateIni, 'yyyy-MM-dd') + ' 00:00:00') +
            '&date2=' + encodeURIComponent(this.dp.transform(dateEnd, 'yyyy-MM-dd') + ' 23:30:00');

        if (userId != '0') {
            urlMethod += '&userId=' + userId;
        }

        if (subsidiaryId != '0') {
            urlMethod += '&subsidiaryId=' + subsidiaryId;
        }

        return this.httpold.get(HttpOptions.getWebApiUrl() + urlMethod,
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

}
