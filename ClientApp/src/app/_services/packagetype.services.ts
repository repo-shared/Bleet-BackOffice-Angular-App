import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import { HttpOptions } from './http.options';
import 'rxjs/add/operator/map';
import { DatePipe } from '@angular/common';

@Injectable()
export class PackageTypeService {
    constructor(private http: Http) { }

    getAll() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/packages/PackageType', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    insert(item) {
        return this.http.post(HttpOptions.getWebApiUrl() + 'api/packages/PackageType', { data: [item] },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    update(item) {
        return this.http.put(HttpOptions.getWebApiUrl() + 'api/packages/PackageType', { data: [item] },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }
}
