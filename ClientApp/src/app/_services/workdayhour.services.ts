import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpOptions } from './http.options';
import { Observable } from 'rxjs';

@Injectable()
export class WorkDayHourService {

    constructor(private http: Http) { }

    getWorkDayHour() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/general/WorkDayHour', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }
    insert(item) {
        return this.http.post(HttpOptions.getWebApiUrl() + 'api/general/WorkDayHour', { data: item },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }
    update(item) {
        return this.http.put(HttpOptions.getWebApiUrl() + 'api/general/WorkDayHour', { data: item },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }
}
