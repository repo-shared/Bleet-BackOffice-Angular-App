import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpOptions } from './http.options';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class SubsidiaryService {
    MarkerPosition$: Observable<any>;
    private MarkerPositionSubject = new Subject<any>();

    Circle$: Observable<any>;
    private CircleSubject = new Subject<any>();

    Marker$: Observable<any>;
    private MarkerSubject = new Subject<any>();

    StartPosition$: Observable<any>;
    private StartPositionSubject = new Subject<any>();

    constructor(private http: Http) {
        this.MarkerPosition$ = this.MarkerPositionSubject.asObservable();
        this.Circle$ = this.CircleSubject.asObservable();
        this.Marker$ = this.MarkerSubject.asObservable();
        this.StartPosition$ = this.StartPositionSubject.asObservable();
    }

    getSubsidiaries() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/general/Subsidiary',
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    insert(item) {
        return this.http.post(HttpOptions.getWebApiUrl() + 'api/general/Subsidiary', { data: [item] },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    update(item) {
        return this.http.put(HttpOptions.getWebApiUrl() + 'api/general/Subsidiary', { data: [item] },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    MarkerPosition(data) {
        this.MarkerPositionSubject.next(data);
    }

    Circle(data) {
        this.CircleSubject.next(data);
    }

    Marker(data) {
        this.MarkerSubject.next(data);
    }

    StartPosition(data) {
        this.StartPositionSubject.next(data);
    }
}
