import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import { HttpOptions } from './http.options';
import 'rxjs/add/operator/map';
import { DatePipe } from '@angular/common';

@Injectable()
export class PackageService {
    constructor(private http: Http) { }

    getAll(userId: string, dateIni: Date, dateEnd: Date) {
        var dp = new DatePipe("en-US");
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/packages/package/GetByDates' +
            '?dateIniStr=' + encodeURIComponent(dp.transform(dateIni, 'yyyy-MM-dd') + ' 00:00:00') +
            '&dateEndStr=' + encodeURIComponent(dp.transform(dateEnd, 'yyyy-MM-dd') + ' 23:30:00') +
            '&idUser=' + userId,
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    getAllByDate(userId: string, date: Date) {
        var dp = new DatePipe("en-US");
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/packages/package/GetByDates' +
            '?dateIniStr=' + encodeURIComponent(dp.transform(date, 'yyyy-MM-dd') + ' 00:00:00') +
            '&dateEndStr=' + encodeURIComponent(dp.transform(date, 'yyyy-MM-dd') + ' 23:30:00') +
            '&idUser=' + userId,
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    getFinalized(userId: string, dateIni: Date, dateEnd: Date) {
        var dp = new DatePipe("en-US");
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/packages/package/GetFinalizedByDates' +
            '?dateIniStr=' + encodeURIComponent(dp.transform(dateIni, 'yyyy-MM-dd') + ' 00:00:00') +
            '&dateEndStr=' + encodeURIComponent(dp.transform(dateEnd, 'yyyy-MM-dd') + ' 00:00:00') +
            '&idUser=' + userId,
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }


    getUserStatus(userId: string, dateIni: Date, dateEnd: Date, subsidiaryId?: number) {
        var dp = new DatePipe("en-US");
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/users/UserStatus/GetActivityLogs' +
            '?date1=' + encodeURIComponent(dp.transform(dateIni, 'yyyy-MM-dd') + ' 00:00:00') +
            '&date2=' + encodeURIComponent(dp.transform(dateEnd, 'yyyy-MM-dd') + ' 00:00:00') +
            '&userId=' + userId,
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    insert(item) {
        return this.http.post(HttpOptions.getWebApiUrl() + 'api/packages/package', { data: [item] },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    update(item) {
        return this.http.put(HttpOptions.getWebApiUrl() + 'api/packages/package', { data: [item] },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }
}
