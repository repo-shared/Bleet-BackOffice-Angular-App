import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import { HttpOptions } from './http.options';
import 'rxjs/add/operator/map';

@Injectable()
export class dashboardDeliveryTypeService {
    constructor(private http: Http) { }

    getAllDelivered() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/packages/RejectionType', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    getAllRejected() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/packages/RejectionType', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    getAllOccupied() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/packages/RejectionType', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    getAllNonOccupied() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/packages/RejectionType', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    getAllRejectionReason() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/packages/RejectionType', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

}
