import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpOptions } from './http.options';
import { DatePipe } from '@angular/common';

@Injectable()
export class LocationService {
    private dp = new DatePipe("en-US");

    constructor(private http: Http) { }

    getDayAllUsersLocation() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/locations/Locations/GetDayAllUsersLocation', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    getDayAllUserLocation(idUser) {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/locations/UserLocation/GetDay?IdUser=' + idUser, HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    getLastLocationByDate(idUser, dateStr) {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/locations/UserLocation/GetLastLocationByDate?idUser=' + idUser + '&dateStr=' + dateStr, HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    /**
     * Get all travel by user and date
     * @param idUser
     * @param date the date has to be and string with this format yyyy-MM-dd
     */
    getByPeriod(idUser: String, date: String) {
        var methodUrl = '/api/locations/UserLocation/GetByPeriod?idUser=' + idUser +
            '&dateIniStr=' + encodeURIComponent(date + ' 00:00:00') +
            '&dateEndStr=' + encodeURIComponent(date + ' 23:30:00');
        return this.http.get(HttpOptions.getWebApiUrl() + methodUrl, HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

}
