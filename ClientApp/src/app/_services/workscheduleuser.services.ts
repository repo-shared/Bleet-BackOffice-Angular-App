import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpOptions } from './http.options';
import { Observable } from 'rxjs';

@Injectable()
export class WorkScheduleUserService {

    constructor(private http: Http) { }

    getUserSchedules() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/general/WorkScheduleUser', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }
    insert(item) {
        console.log(JSON.stringify({ data: item }));
        return this.http.post(HttpOptions.getWebApiUrl() + 'api/general/WorkScheduleUser', { data: item },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }
    update(item) {
        console.log(JSON.stringify({ data: item }));
        return this.http.put(HttpOptions.getWebApiUrl() + 'api/general/WorkScheduleUser', { data: item },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }
}
