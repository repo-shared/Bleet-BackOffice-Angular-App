import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import { HttpOptions } from './http.options';
import 'rxjs/add/operator/map';

@Injectable()
export class RejectionTypeService {
    constructor(private http: Http) { }

    getAll() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/packages/RejectionType', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    insert(item) {
        return this.http.post(HttpOptions.getWebApiUrl() + 'api/packages/RejectionType', { data: [item] },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    update(item) {
        return this.http.put(HttpOptions.getWebApiUrl() + 'api/packages/RejectionType', { data: [item] },
            HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }
}
