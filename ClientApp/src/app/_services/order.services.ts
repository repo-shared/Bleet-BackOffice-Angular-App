import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpOptions } from './http.options';
import { DatePipe } from '@angular/common';

@Injectable()
export class OrderService {
    private dp = new DatePipe("en-US");

    constructor(private http: HttpClient) { }

    getAll(): Observable<Orders[]> {
        return this.http.get<Orders[]>(HttpOptions.getWebApiUrl() + 'api/order/AllOrders', HttpOptions.getRequestOptionsHttpClient());
    }

   
}