
import { HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import { LocalStorageUtil } from '../utils/localstorageutil';

export class HttpOptions {

    public static getRequestOptions() {
        const LoggedInUser = LocalStorageUtil.getUserLoginData().authToken;
        const mHeaders = new Headers({ 'Content-Type': 'application/json' });
        const auth = 'Bearer ' + LoggedInUser;
        mHeaders.append('Authorization', auth);
        const options = new RequestOptions({
            headers: mHeaders
        });

        return options;
    }


    public static getRequestOptionsHttpClient() {
        const LoggedInUser = LocalStorageUtil.getUserLoginData().authToken;
        const mHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
        const auth = 'Bearer ' + LoggedInUser;
        mHeaders.append('Authorization', auth);
        const options = {
            headers: mHeaders
        };

        return options;
    }

    public static getWebApiUrl() {
        return environment.WebApiUrl;
    }

}


