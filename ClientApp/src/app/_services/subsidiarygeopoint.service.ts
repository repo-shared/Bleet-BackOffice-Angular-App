import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpOptions } from './http.options';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class SubsidiaryGeoPointService {

    Polygon$: Observable<any>;
    private PolygonSubject = new Subject<any>();

    constructor(private http: Http) {
        this.Polygon$ = this.PolygonSubject.asObservable();
    }

    getSubsidiariesGeoPoints() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/general/SubsidiaryGeoPoint', HttpOptions.getRequestOptions()).map((res: Response) => res.json());
    }

    Polygon(data) {
        this.PolygonSubject.next(data);
    }
}