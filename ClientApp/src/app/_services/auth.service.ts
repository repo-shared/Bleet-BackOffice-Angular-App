import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from './user.services';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private myRoute: Router, private userService: UserService) { }

    sendToken(token: string) {
        localStorage.setItem('LoggedInUser', token);
    }

    getToken() {
        return localStorage.getItem('LoggedInUser');
    }

    removeToken() {
        return localStorage.removeItem('LoggedInUser');
    }

    logout() {
        localStorage.removeItem('LoggedInUser');
        this.myRoute.navigate(['Login']);
    }

}
