import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpOptions } from './http.options';
import { Observable } from 'rxjs';

@Injectable()
export class RegionService {

    constructor(private http: Http) { }

    getRegions() {
        return this.http.get(HttpOptions.getWebApiUrl() + 'api/general/Region', HttpOptions.getRequestOptions()).map((res: Response) => res.json());

        //return Observable.create(observer => {
        //    observer.next({
        //        "data": [{
        //            "id": "1",
        //            "regionName": "ZONA CZ 1 - QUEMADA",
        //            "idRegion": "1"
        //        },
        //        {
        //            "id": "2",
        //            "regionName": "ZONA CZ 2 - QUEMADA",
        //            "idRegion": "2"
        //        }]
        //    });
        //    observer.complete();
        //});
    }
}
