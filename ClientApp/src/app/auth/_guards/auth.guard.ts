import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Rx";
import { LocalStorageUtil } from '../../utils/localstorageutil';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private _router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        let LoggedInUser = LocalStorageUtil.getUserLoginData().authToken;

        if (LoggedInUser != "" && LoggedInUser != null) {
            return true;
        } else {
            this._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
            return false;
        }

    }
}