import {
    Component,
    ComponentFactoryResolver,
    OnInit,
    ViewChild,
    ViewContainerRef,
    ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScriptLoaderService } from '../_services/script-loader.service';
import { UserService } from '../_services/user.services';
import { LoginCustom } from './_helpers/login-custom';
import { Helpers } from '../helpers';
import { Notification } from '../utils/notification';
import { LocalStorageUtil } from '../utils/localstorageutil';
import { FullLayout } from '../theme/theme-routes';
import { PerfilUser } from '../utils/perfil.user.enum';
import { Http } from '@angular/http';

@Component({
    selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
    templateUrl: './templates/login-1.component.html',
    encapsulation: ViewEncapsulation.None,
})

export class AuthComponent implements OnInit {
    model: any = {
        userName: "",
        password: ""
    };
    loading = false;
    returnUrl: string;

    constructor(
        private _router: Router,
        private _script: ScriptLoaderService,
        private _userService: UserService,
        private _route: ActivatedRoute,
        private cfr: ComponentFactoryResolver) {

           
    }

    ngOnInit() {
        this.model.remember = true;
        // get return url from route parameters or default to '/'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
        /*this._router.navigate([this.returnUrl]);
*/
        /* this._script.loadScripts('body', [
             'assets/vendors/base/vendors.bundle.js',
             'assets/demo/default/base/scripts.bundle.js'], true).then(() => {
                 Helpers.setLoading(false);
                 LoginCustom.init();
             });*/
    }

    signin() {
        this.loading = true;
        this._userService.Login(this.model).subscribe(data => {
            /*data.rol = 0;
            if (data.roleCode == 'ADMIN') {
                data.rol = PerfilUser.Admin;
            }

            if (data.roleCode == 'SUPER') {
                data.rol = PerfilUser.SuperUser;
            }

            if (data.roleCode == 'COORD') {
                data.rol = PerfilUser.Coordinator;
            }

            if (data.rol == 0) {
                Notification.Error('El usuario no tiene permiso a acceder a la plataforma.');
                this.loading = false;

            } else {
                LocalStorageUtil.setUserLoginData(data);
                this._router.config[3].children = FullLayout.getFullRoutes(data.rol);

                this._router.navigate([this.returnUrl]);
            }*/

            if(data.length > 0){
                var elem = data[0];
                elem.authToken = "12345";
                elem.rol = PerfilUser.Admin;
    
                LocalStorageUtil.setUserLoginData(elem);
                
                this._router.config[3].children = FullLayout.getFullRoutes(PerfilUser.Admin);
    
                this._router.navigate([this.returnUrl]);
            }else{
                Notification.Error('El usuario no tiene permiso a acceder a la plataforma.');
                this.loading = false;
            }
           
        },
            error => {
                Notification.WebMethodError(error);
                this.loading = false;
            });
    }

    signup() {
        /*this.loading = true;
        this._userService.create(this.model).subscribe(
            data => {
                this.showAlert('alertSignin');
                this._alertService.success(
                    'Thank you. To complete your registration please check your email.',
                    true);
                this.loading = false;
                LoginCustom.displaySignInForm();
                this.model = {};
            },
            error => {
                this.showAlert('alertSignup');
                this._alertService.error(error);
                this.loading = false;
            });*/
    }

    forgotPass() {/*
        this.loading = true;
        this._userService.forgotPassword(this.model.email).subscribe(
            data => {
                this.showAlert('alertSignin');
                this._alertService.success(
                    'Cool! Password recovery instruction has been sent to your email.',
                    true);
                this.loading = false;
                LoginCustom.displaySignInForm();
                this.model = {};
            },
            error => {
                this.showAlert('alertForgotPass');
                this._alertService.error(error);
                this.loading = false;
            });*/
    }


}