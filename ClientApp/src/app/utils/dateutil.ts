export class DateUtil {

    //Get String String from date, example: 2018-01-01 00:00:00
    public static getStringDate(date) {
        var strDate = date.getFullYear() + "-" + this.fill2ZerosLeft(date.getMonth() + 1) + "-" + this.fill2ZerosLeft(date.getDate()) + " " + this.fill2ZerosLeft(date.getHours()) + ":" + this.fill2ZerosLeft(date.getMinutes()) + ":00";
        return strDate;
    }

    public static getWeekDayList() {

        var dayList = new Array();
        dayList.push({ id: 1, day: 'Lunes' });
        dayList.push({ id: 2, day: 'Martes' });
        dayList.push({ id: 3, day: 'Mi\u00e9rcoles' });
        dayList.push({ id: 4, day: 'Jueves' });
        dayList.push({ id: 5, day: 'Viernes' });
        dayList.push({ id: 6, day: 'S\u00e1bado' });
        dayList.push({ id: 7, day: 'Domingo' });

        return dayList;
    }

    public static getWeekDayById(id) {
        var dayList = DateUtil.getWeekDayList();

        if (id == 0) {
            id = 1;
        }
        return dayList.filter(x => x.id == id)[0];
    }

    private static fill2ZerosLeft(value: string) {
        if (value.length == 1) {
            return '0' + value;
        } else {
            return value;
        }
    }

}


