export enum PerfilUser {
    Admin = 1,
    SuperUser = 2,
    Coordinator = 3
};
