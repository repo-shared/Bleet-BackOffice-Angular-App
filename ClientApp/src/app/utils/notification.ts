import notify from 'devextreme/ui/notify'

export class Notification {

    private static time: number = 2000;

    public static Error(message) {
        notify(message, "error", this.time);
    }

    public static CustomError(message, time: number) {
        notify(message, "error", time);
    }

    public static Success(message) {
        notify(message, "success", this.time);
    }


    //Notify Error from error consuming web api
    public static WebMethodError(error) {
        let message = "";

        if (error.status == 400) {
            message = error._body;
        }

        if (error.status == 401) {
            message = "No esta autorizado para realizar esta acción.";
        }

        if (error.status == 404) {
            message = "No se encontró el registro o metodo buscado.";
        }

        if (error.status == 0) {
            message = "Verifique su conexión a internet e itentelo de nuevo.";
        }

        if (message != "") {
            notify(message, "error", this.time);
            return true;
        } else {
            return false;
        }

    }

    //Get String NotificationError
    public static WebMethodErrorString(error) {

        if (error.status == 400) {
            return error._body;
        }

        if (error.status == 401) {
            return "No esta autorizado para realizar esta acción.";
        }

        if (error.status == 404) {
            return "No se encontró el registro o metodo buscado.";
        }

        if (error.status == 0) {
            return "Verifique su conexión a internet e itentelo de nuevo.";
        }

        return "Ocurrió un error.";

    }


}


