import * as $ from "jquery";


export class LocalStorageUtil {

    public static setUserLoginData(userLoginData) {
        localStorage.setItem('userLoginData', JSON.stringify(userLoginData));
    }

    public static getUserLoginData() {
        var objs = localStorage.getItem('userLoginData');
        var obj = $.parseJSON(objs);
        if (obj == null)
            obj = {
                authToken: ""
            };

        return obj;
    }

}

