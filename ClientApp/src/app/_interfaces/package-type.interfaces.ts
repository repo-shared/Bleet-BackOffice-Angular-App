export interface PackageType {
    id: number,
    idCompany: number,
    typeName: string,
    active: boolean
}