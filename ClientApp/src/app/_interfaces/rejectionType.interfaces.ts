export interface RejectionType {
    id: number,
    idCompany: number,
    rejectionName: string,
    active: boolean
}