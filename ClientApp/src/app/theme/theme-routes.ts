import { Routes, RouterModule } from '@angular/router';
import { PerfilUser } from '../utils/perfil.user.enum';

export class FullLayout {

    public static getFullRoutes(rol: PerfilUser) {
        let routes: Routes = [];

        routes = FULL_ROUTES.filter(function(r) {
            return r.data.find(function(data) {
                return data.rol === 0 || data.rol === rol;
            });
        });

        return routes;
    }

}

/*
export const FULL_ROUTES: Routes = [
    {
        "path": "index",
        "loadChildren": () => import('./pages/default/index/index.module').then(m => m.IndexModule),
        "data": [{ rol: 0 }]
    },
    {
        "path": "dashboard",
        "loadChildren": () => import('./pages/default/components/dashboard/dashboard.deliveries.module').then(m => m.DashboardDeliveryModule),
        "data": [{ rol: 0 }]
    },
    {
        "path": "maps\/locations",
        "loadChildren": () => import('./pages/default/components/maps/locations/locations.module').then(m => m.LocationsModule),
        "data": [{ rol: 0 }]
    },
    {
        "path": 'maps\/locations\/travel\/:id\/:dstr',
        "loadChildren": () => import('./pages/default/components/maps/travel/travel.module').then(m => m.TravelModule),
        "data": [{ rol: 0 }]
    },
    {
        "path": "admin\/users",
        "loadChildren": () => import('./pages/default/components/admin/users/users.module').then(m => m.UsersModule),
        "data": [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
    },
    {
        "path": "admin\/packages",
        "loadChildren": () => import('./pages/default/components/admin/packages/packages.module').then(m => m.PackagesModule),
        "data": [{ rol: 0 }]
    },
    {
        "path": "admin\/packagetype",
        "loadChildren": () => import('./pages/default/components/admin/packagetypes/packagetypes.module').then(m => m.PackageTypesModule),
        "data": [{ rol: 0 }]
    },
    {
        "path": "admin\/rejectionType",
        "loadChildren": () => import('./pages/default/components/admin/rejectionType/rejectionType.module').then(m => m.RejectionTypeModule),
        "data": [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
    },
    {
        "path": "subsidiaries\/subsidiary",
        "loadChildren": () => import('./pages/default/components/subsidiaries/subsidiary/subsidiary.module').then(m => m.SubsidiaryModule),
        "data": [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
    },
    {
        "path": "subsidiaries\/subsidiary\/data\/:subsidiaryid",
        "loadChildren": () => import('./pages/default/components/subsidiaries/data/data.module').then(m => m.DataModule),
        "data": [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
    },
    {
        "path": "report\/deliveriesmade",
        "loadChildren": () => import('./pages/default/components/reports/deliveriesmade/deliveriesmade.module').then(m => m.DeliveriesMadeModule),
        "data": [{ rol: PerfilUser.Admin }, { rol: PerfilUser.Coordinator }]
    },
    {
        "path": "report\/deliveriesreject",
        "loadChildren": () => import('./pages/default/components/reports/deliveriesreject/deliveriesreject.module').then(m => m.DeliveriesRejectModule),
        "data": [{ rol: PerfilUser.Admin }, { rol: PerfilUser.Coordinator }]
    },
    {
        "path": "report\/log",
        "loadChildren": () => import('./pages/default/components/reports/log/log.module').then(m => m.LogModule),
        "data": [{ rol: PerfilUser.Admin }, { rol: PerfilUser.Coordinator }]
    },
    {
        "path": "report\/milestonecontrol",
        "loadChildren": () => import('./pages/default/components/reports/milestonecontrol/milestonecontrol.module').then(m => m.MilestoneControlModule),
        "data": [{ rol: PerfilUser.Admin }, { rol: PerfilUser.Coordinator }]
    },
    {
        "path": "404",
        "loadChildren": () => import('./pages/default/not-found/not-found.module').then(m => m.NotFoundModule),
        "data": [{ rol: 0 }]
    },
    {
        "path": "admin\/schedules",
        "loadChildren": () => import('./pages/default/components/admin/schedules/schedules.module').then(m => m.SchedulesModule),
        "data": [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
    },
    {
        "path": "",
        "redirectTo": "index",
        "pathMatch": "full",
        "data": [{ rol: 0 }]
    }
];
*/

export const FULL_ROUTES: Routes = [
    {
        "path": "index",
        "loadChildren": () => import('./pages/default/index/index.module').then(m => m.IndexModule),
        "data": [{ rol: 0 }]
    },
    {
        "path": "dashboard",
        "loadChildren": () => import('./pages/default/components/dashboard/dashboard.deliveries.module').then(m => m.DashboardDeliveryModule),
        "data": [{ rol: 0 }]
    },
    {
        "path": "admin\/users",
        "loadChildren": () => import('./pages/default/components/admin/users/users.module').then(m => m.UsersModule),
        "data": [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
    },
    {
        "path": "admin\/products",
        "loadChildren": () => import('./pages/default/components/admin/products/products.module').then(m => m.ProductsModule),
        "data": [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
    },
    {
        "path": "admin\/packages",
        "loadChildren": () => import('./pages/default/components/admin/packages/packages.module').then(m => m.PackagesModule),
        "data": [{ rol: 0 }]
    },
    {
        "path": "admin\/rejectionType",
        "loadChildren": () => import('./pages/default/components/admin/rejectionType/rejectionType.module').then(m => m.RejectionTypeModule),
        "data": [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
    },
    {
        "path": "404",
        "loadChildren": () => import('./pages/default/not-found/not-found.module').then(m => m.NotFoundModule),
        "data": [{ rol: 0 }]
    },
    {
        "path": "",
        "redirectTo": "index",
        "pathMatch": "full",
        "data": [{ rol: 0 }]
    }
];