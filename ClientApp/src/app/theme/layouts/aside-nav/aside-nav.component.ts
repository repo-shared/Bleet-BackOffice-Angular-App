import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { LocalStorageUtil } from '../../../utils/localstorageutil';
import { ROUTES_FULL } from './aside-nav.routes.config';

declare let mLayout: any;

@Component({
    selector: "app-aside-nav",
    templateUrl: "./aside-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class AsideNavComponent implements OnInit, AfterViewInit {

    public menuItems: any[];
    constructor() {

    }

    ngOnInit() {
        this.getRoutes();
    }

    getRoutes() {
        this.menuItems = ROUTES_FULL.filter(function(r) {
            return r.data.find(function(data) {
                return data.rol === 0 || data.rol === LocalStorageUtil.getUserLoginData().rol;
            });
        });

    }

    ngAfterViewInit() {

        mLayout.initAside();

    }

}