import { RouteInfo } from './aside-nav.metadata';
import { PerfilUser } from '../../../utils/perfil.user.enum';

export const ROUTES_FULL: RouteInfo[] = [
    {
        path: '/index', title: 'Dashboard', icon: 'm-menu__link-icon flaticon-line-graph', class: 'm-menu__item', badge: '', badgeClass: '', isExternalLink: false,
        submenu: [],
        data: [{ rol: 0 }]
    },
    {
        path: '', title: 'Configuraci&oacute;n', icon: 'm-menu__link-icon flaticon-cogwheel-2', class: 'm-menu__item  m-menu__item--submenu', badge: '', badgeClass: '', isExternalLink: false,
        submenu: [
            {
                path: '/admin/users', title: 'Usuarios',
                icon: 'm-menu__link-bullet m-menu__link-bullet--dot', class: 'm-menu__item', badge: '', badgeClass: '', isExternalLink: false, submenu: [],
                data: [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
            },
            {
                path: '/admin/rejectionType', title: 'Motivos de Rechazo',
                icon: 'm-menu__link-bullet m-menu__link-bullet--dot', class: 'm-menu__item', badge: '', badgeClass: '', isExternalLink: false, submenu: [],
                data: [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
            },
            {
                path: '/admin/products', title: 'Productos',
                icon: 'm-menu__link-bullet m-menu__link-bullet--dot', class: 'm-menu__item', badge: '', badgeClass: '', isExternalLink: false, submenu: [],
                data: [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
            }
        ],
        data: [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
    },
    {
        path: '', title: 'Pedidos', icon: 'm-menu__link-icon flaticon-open-box', class: 'm-menu__item  m-menu__item--submenu', badge: '', badgeClass: '', isExternalLink: false,
        submenu: [
            {
                path: '/admin/packages', title: 'Pedidos Realizados',
                icon: 'm-menu__link-bullet m-menu__link-bullet--dot', class: 'm-menu__item', badge: '', badgeClass: '', isExternalLink: false, submenu: [],
                data: [{ rol: 0 }]
            },
        ],
        data: [{ rol: 0 }]
    },
   
    {
        path: '', title: 'Reportes', icon: 'm-menu__link-icon flaticon-line-graph', class: 'm-menu__item  m-menu__item--submenu', badge: '', badgeClass: '', isExternalLink: false,
        submenu: [
            {
                path: '/report/deliveriesmade', title: 'Entregas Realizadas',
                icon: 'm-menu__link-bullet m-menu__link-bullet--dot', class: 'm-menu__item', badge: '', badgeClass: '', isExternalLink: false, submenu: [],
                data: [{ rol: PerfilUser.Admin }, { rol: PerfilUser.Coordinator }]
            },
            {
                path: '/report/deliveriesreject', title: 'Entregas Rechazadas',
                icon: 'm-menu__link-bullet m-menu__link-bullet--dot', class: 'm-menu__item', badge: '', badgeClass: '', isExternalLink: false, submenu: [],
                data: [{ rol: PerfilUser.Admin }, { rol: PerfilUser.Coordinator }]
            },
        ],
        data: [{ rol: PerfilUser.Admin }, { rol: PerfilUser.SuperUser }]
    },
    {
        path: '/logout', title: 'Cerrar', icon: 'm-menu__link-icon flaticon-logout', class: 'm-menu__item', badge: '', badgeClass: '', isExternalLink: false, submenu: [],
        data: [{ rol: 0 }]
    }
];