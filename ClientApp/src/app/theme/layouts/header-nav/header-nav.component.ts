import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { LocalStorageUtil } from '../../../utils/localstorageutil';
import { PerfilUser } from '../../../utils/perfil.user.enum';

declare let mLayout: any;
@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {

    public userLoginData = LocalStorageUtil.getUserLoginData();

    constructor() {

    }
    ngOnInit() {

    }
    ngAfterViewInit() {

        mLayout.initHeader();

    }

    public isCoordinatorAdmin() {
        return this.userLoginData.rol == PerfilUser.Coordinator || this.userLoginData.rol == PerfilUser.Admin;
    }



}