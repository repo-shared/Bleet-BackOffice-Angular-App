import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { DatePipe } from '@angular/common';
import { } from 'googlemaps';
import { LocationComponentHelper } from './locations.component.helper'
import { LocationService } from '../../../../../../_services/location.services';
import { SubsidiaryService } from '../../../../../../_services/subsidiary.service';
import { UserService } from '../../../../../../_services/user.services';
import { Notification } from '../../../../../../utils/notification';
import { PackageService } from '../../../../../../_services/package.services';
import { LocalStorageUtil } from '../../../../../../utils/localstorageutil';
import { DateUtil } from '../../../../../../utils/dateutil';
import { PerfilUser } from '../../../../../../utils/perfil.user.enum';


@Component({
    selector: "app-maps-locations",
    templateUrl: "./locations.component.html",
    encapsulation: ViewEncapsulation.None
})

export class LocationsComponent implements OnInit, AfterViewInit {

    @ViewChild('gmap') mapElement: ElementRef;
    map: google.maps.Map;

    private interval: any;
    private locationHelper: LocationComponentHelper;
    public userLoginData = LocalStorageUtil.getUserLoginData();

    public showUserList: boolean = true;

    public mostrarTodo: boolean = true;
    public points: any;

    public popupFiltrosVisible: boolean = false;
    public loadingPopupFiltrosVisible: boolean = false;

    public subSidiaryList: any;
    public userList: any;
    public packagesFinalized: any;
    public packagesAll: any;
    public now: Date = new Date();

    public userAllSelected: Boolean = false

    public filters: any = {
        startDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()),
        idUser: '0',
        idSubsidiary: '0',
        betterLocation: false
    };

    public realTime: boolean = false;
    public isLoaded: boolean = false;
    public datePipe: DatePipe

    public locationService: LocationService
    public subsidiaryService: SubsidiaryService
    public userService: UserService

    constructor(locationService: LocationService, subsidiaryService: SubsidiaryService, userService: UserService, datePipe: DatePipe
        , private pkgsSrv: PackageService) {

        this.locationService = locationService;
        this.subsidiaryService = subsidiaryService;
        this.userService = userService;
        this.datePipe = datePipe;

    }

    ngOnInit() {
        this.subsidiaryService.getSubsidiaries().subscribe(
            result => {
                this.subSidiaryList = result.data;

                this.subSidiaryList.unshift({ id: 0, name: 'Todas' });

                if (this.userLoginData.rol == PerfilUser.Coordinator) {

                    var item = this.subSidiaryList.filter(s => s.name == this.userLoginData.subsidiaryName)[0];

                    if (item)
                        this.filters.idSubsidiary = item.id;

                }

            },
            error => {
                Notification.WebMethodError(error);
            });

    }

    ngOnDestroy() {
        if (this.interval) {
            clearTimeout(this.interval);
        }
    }

    refreshLocations() {
        Helpers.setLoading(true, 'Cargando Ubicaciones...');
        this.packagesFinalized = null;
        this.pkgsSrv.getFinalized('', this.addDays(new Date(), -7), this.addDays(new Date(), 1)).subscribe(res => {
            if (res.data) { this.packagesFinalized = res.data; }
            Helpers.setLoading(true, 'Cargando Ubicaciones...');
            this.queryLocations();
        });



    }

    queryLocations() {

        this.locationService.getLastLocationByDate('0', this.datePipe.transform(this.filters.startDate, "yyyy-MM-dd")).subscribe(
            result => {
                var res;
                //if (this.userList != null) {
                //    let brr = this.userList.filter(f => f.selected == true).map(i => { return i.id; });
                //    res = result.data.filter(f => brr.includes(f.idUser));
                //} else {
                var res = result.data;
                //}

                var data = new Array();
                result.data.forEach(function(item) {
                    data.push({ id: item.idUser, fullName: item.userName, selected: false, state: 'Disponible', lat: item.latitude, lng: item.longitude });
                });

                this.userService.getActivityLogs(this.filters.startDate, this.filters.startDate, '0', '0').subscribe(
                    result => {

                        data.forEach(el => {

                            var item = result.data.filter(d => d.userName == el.fullName)[0];
                            if (item != undefined) {

                                var resUser = res.filter(d => d.userName == el.fullName)[0];
                                if (resUser != undefined) {
                                    resUser.userName = item.userFullName;
                                }

                                el.fullName = item.userFullName;
                                el.state = item.statusDescription;
                            }


                        });

                        this.userList = data;
                        this.isLoaded = this.locationHelper.addUsers(res, this.isLoaded, this.datePipe.transform(this.filters.startDate, 'yyyy-MM-dd'));
                    },
                    error => {
                        Notification.WebMethodError(error);
                        Helpers.setLoading(false);
                        this.interval = setTimeout(() => {
                            this.refreshLocations();
                        }, 10000);
                    }
                );




                if (this.packagesFinalized) {
                    this.packagesFinalized.forEach(element => {
                        if (element.latitude == 0 || element.longitude == 0
                            || element.latitude == "0" || element.longitude == "0") { return; }
                        element.traceDate = this.datePipe.transform(element.traceDate, "dd/MM/yyyy hh:mm a");
                        this.locationHelper.addPackageMarker(element);
                    });
                }
                Helpers.setLoading(false);
                this.interval = setTimeout(() => {
                    this.refreshLocations();
                }, 10000);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
                this.interval = setTimeout(() => {
                    this.refreshLocations();
                }, 10000);
            }
        );
    }

    addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }

    ngAfterViewInit() {


        const mapProperties = {
            center: new google.maps.LatLng(this.userLoginData.latitude, this.userLoginData.longitude),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);

        this.locationHelper = new LocationComponentHelper(this.map);

        this.refreshLocations();
    }

    subsidiary_onValueChanged(e) {
        console.log(e.value);

        Helpers.setLoading(true, 'Cargando Usuarios...');
        var method: any;
        if (e.value == 0) {
            method = this.userService.getAll();
        } else {
            method = this.userService.getAllBySubsidiary(e.value);
        }

        method.subscribe(
            result => {
                var data = new Array();
                result.data.forEach(function(item) {
                    data.push({ id: item.id, fullName: item.fullName, selected: false, state: 'Disponible' });
                });

                this.userList = data;

                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });
    }

    mapa_onValueChanged(e) {
        console.log(e)
    }

    mapa_filter() {
        if (this.popupFiltrosVisible) {
            this.popupFiltrosVisible = false;
        }

        if (this.interval) {
            clearTimeout(this.interval);
        }
        this.refreshLocations();
    }

    //filter
    todo_user_selected(e) {
        this.userList.forEach(function(item) {
            item.selected = e.value;
        });

    }

    get_state_color(state) {
        if (state == "Disponible") {
            return "green";
        }

        if (state == "En ruta") {
            return "yellow";
        }

        if (state == "SIN ACTIVIDAD") {
            return "red";
        }
    }

    user_onSelectionChanged(e) {

        var info = e.selectedRowsData[0];
        this.locationHelper.centerMap(info.lat, info.lng);
    }

    isCoordinator() {
        return this.userLoginData.rol == PerfilUser.Coordinator;
    }
}