import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { } from 'googlemaps';
import { TravelComponentHelper } from './travel.component.helper'
import { LocationService } from '../../../../../../_services/location.services';
import { Notification } from '../../../../../../utils/notification';
import { UserService } from '../../../../../../_services/user.services';
import { LocalStorageUtil } from '../../../../../../utils/localstorageutil';

@Component({
    selector: "app-maps-travel",
    templateUrl: "./travel.component.html",
    encapsulation: ViewEncapsulation.None
})

export class TravelComponent implements OnInit, AfterViewInit {

    @ViewChild('gmap') mapElement: ElementRef;
    map: google.maps.Map;

    private intervalTravel: any;

    private locationHelper: TravelComponentHelper;

    public mostrarTodo: boolean = true;
    public points: any;

    public popupFiltrosVisible: boolean = false;
    public loadingPopupFiltrosVisible: boolean = false;
    public isLoaded: boolean = false;

    public userList: any;
    public now: Date = new Date();


    public filters: any = {
        startDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()),
        endDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate(), 23, 59, 59),
        idUser: '',
        betterLocation: false
    };

    public realTime: boolean = false;
    public datePipe: DatePipe

    public locationService: LocationService;
    public usersService: UserService;
    crrntUserId: any;
    crrntUserDate: any;
    crrntUserName: any;
    constructor(locationService: LocationService, datePipe: DatePipe, private route: ActivatedRoute, private router: Router, usersService: UserService) {
        console.log('Init getting data');

        this.locationService = locationService;
        this.usersService = usersService;
        this.datePipe = datePipe;

    }

    ngOnInit() {

    }

    ngOnDestroy() {
        if (this.intervalTravel) {
            clearTimeout(this.intervalTravel);
        }
    }

    getUserData() {
        this.route.params.subscribe(params => {
            this.usersService.getAll().subscribe(
                result => {
                    this.crrntUserName = "";
                    result.data.forEach(element => {
                        if (params['id'] == element.id) {
                            this.crrntUserName = element.userName;
                        }
                    });;
                    this.refreshLocations();
                },
                error => {
                    Notification.WebMethodError(error);
                    Helpers.setLoading(false);
                    /*this.intervalTravel = setTimeout(() => {
                        this.refreshLocations(); 
                    }, 10000);*/
                });
        });
    }

    refreshLocations() {
        Helpers.setLoading(true, 'Cargando Recorridos...');

        this.locationService.getByPeriod(this.crrntUserId, this.crrntUserDate).subscribe(
            result => {
                this.isLoaded = this.locationHelper.drawRoute(result.data, this.isLoaded, this.crrntUserName);
                Helpers.setLoading(false);
                this.intervalTravel = setTimeout(() => {
                    this.refreshLocations();
                }, 10000);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
                this.intervalTravel = setTimeout(() => {
                    this.refreshLocations();
                }, 10000);
            });
    }

    ngAfterViewInit() {

        let userLoginData = LocalStorageUtil.getUserLoginData();

        const mapProperties = {
            center: new google.maps.LatLng(userLoginData.latitude, userLoginData.longitude),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);

        this.locationHelper = new TravelComponentHelper(this.map);

        this.route.params.subscribe(params => {
            this.crrntUserId = params['id'];
            this.crrntUserDate = params['dstr'];
            console.log(this.crrntUserId);
            this.getUserData();
        });



    }

    mapa_onValueChanged(e) {
        console.log(e)
    }

    mapa_filter() {

        Helpers.setLoading(true, 'Cargando Ubicaciones...');

        this.locationService.getDayAllUsersLocation().subscribe(
            result => {
                alert(result);
                Helpers.setLoading(false);
            },
            error => {
                alert(error);
                Helpers.setLoading(false);
            });

    }

}