import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { Routes, RouterModule } from '@angular/router';

import { LocationsComponent } from "./locations.component";

import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';

import { LocationService } from '../../../../../../_services/location.services'
import { SubsidiaryService } from '../../../../../../_services/subsidiary.service'
import { UserService } from '../../../../../../_services/user.services'
import { PackageService } from '../../../../../../_services/package.services';

import { DxDataGridModule, DxListModule, DxSwitchModule, DxPopupModule, DxCheckBoxModule, DxTextBoxModule, DxLookupModule, DxDateBoxModule, DxValidatorModule, DxLoadPanelModule } from 'devextreme-angular';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": LocationsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, DxListModule,
        DxSwitchModule,
        DxPopupModule,
        DxLookupModule,
        DxDateBoxModule,
        DxTextBoxModule,
        DxLoadPanelModule,
        DxValidatorModule,
        DxCheckBoxModule,
        DxDataGridModule,
        FormsModule
    ], exports: [
        RouterModule
    ], declarations: [
        LocationsComponent
    ],
    providers: [LocationService, SubsidiaryService, UserService, DatePipe, PackageService],
})
export class LocationsModule {



}