import { } from 'googlemaps';

export class LocationComponentHelper {

    private map: google.maps.Map;

    private iconStartUrl = '/assets/app/media/img/location/moto.png';
    private iconPackageAccepted = '/assets/app/media/img/packages/Package-accept-icon.png';
    private iconPackageRejected = '/assets/app/media/img/packages/Package-delete-icon.png';

    private markers = [];

    private infoWindow: google.maps.InfoWindow;

    constructor(map: google.maps.Map) {
        this.map = map;
        this.infoWindow = new google.maps.InfoWindow();
    }

    cleanMap() {
        this.markers.forEach(function(marker) {
            marker.setMap(null);
        });
        this.markers = [];
    }


    centerMap(lat, lng) {
        var lastPoint = { lat: lat, lng: lng };
        this.map.setCenter(lastPoint);
    }

    addPackageMarker(element: any) {
        var _marker = new google.maps.Marker({
            map: this.map,
            position: { lat: element.latitude, lng: element.longitude },
            icon: (element.statusId == 4 ? this.iconPackageAccepted : this.iconPackageRejected),
            draggable: false,
            title: element.userName
        });
        this.markers.push(_marker);
        let _this = this;
        var contentString = '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading">' + element.packageCode + " - " + element.clientName + '</h1>' +
            '<div id="bodyContent">' +
            '<p><i class="fa fa-info"></i>&nbsp; ' + element.statusName + ' por ' + element.userName + '.</p>' +
            '<p><b><i class="fa fa-calendar"></i>&nbsp;Fecha Hora</b>: ' + element.traceDate + '.</p>' +
            '</div>';
        google.maps.event.addListener(_marker, "click", function(e) {
            _this.infoWindow.setContent(contentString);
            _this.infoWindow.open(_this.map, _marker);
        });
    }

    addInfoWindowsToUserMarker(marker: google.maps.Marker, user: any, date: string) {

        let _this = this;

        var paquetes = "<table class='table'><thead><th>Cliente</th><th>Direcci&oacute;n</th><th>Tel&eacute;fono</th></thead><tbody>";

        user.packages.forEach(el => {
            if (el.idPackageStatus == 1)
                paquetes += "<tr><td>" + el.clientName + "</td><td>" + el.clientAddress + "</td><td>" + el.clientPhone + "</td></tr>"
        });

        paquetes += "</tr></tbody></table>";

        var contentString = '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h3 id="firstHeading" class="firstHeading">' + user.userName + '</h3>' +
            '<div id="bodyContent">' +
            '<p><b><i class="fa fa-calendar"></i>&nbsp;Fecha Hora</b>: ' + user.mobilDate + '.</p>' +
            '<div style="text-align: center"><a href="#/maps/locations/travel/' + user.idUser + '/' + date + '"  class="btn btn-default"><i class="fa fa-map"></i>&nbsp;Ver Recorrido</a></div>' +
            '<hr/><h5><i class="fa fa-archive"></i>&nbsp;Paquetes Pendientes</h5>' +
            paquetes +
            '</div>';

        google.maps.event.addListener(marker, "click", function(e) {
            _this.infoWindow.setContent(contentString);
            _this.infoWindow.open(_this.map, marker);
        });

    }

    addUsers(users: any, loaded: boolean = false, date): boolean {
        var rst = false;
        this.cleanMap();

        var lastPoint = null;
        users.forEach(element => {

            lastPoint = { lat: element.latitude, lng: element.longitude };
            var _marker = new google.maps.Marker({
                map: this.map,
                //animation: google.maps.Animation.BOUNCE,
                position: lastPoint,
                icon: this.iconStartUrl,
                draggable: false,
                title: element.userName,
                zIndex: 999
            });

            this.markers.push(_marker);
            this.addInfoWindowsToUserMarker(_marker, element, date);
            rst = true;


        });

        if (!loaded) {
            this.map.setCenter(lastPoint);
            this.map.setZoom(13);
        }
        return rst;
    }




}