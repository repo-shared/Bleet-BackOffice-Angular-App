import { } from 'googlemaps';

export class TravelComponentHelper {

    private map: google.maps.Map;

    private iconStartUrl = '/assets/app/media/img/location/pinStart.png';
    private iconEndUrl = '/assets/app/media/img/location/moto.png';

    private firstMarker = null;
    private lastMarker = null;

    private linesPath = null;

    private infoWindow: google.maps.InfoWindow;

    constructor(map: google.maps.Map) {
        this.map = map;
        this.infoWindow = new google.maps.InfoWindow();
    }

    cleanMap() {
        if (this.firstMarker != null) {
            this.firstMarker.setMap(null);
        }

        if (this.lastMarker != null) {
            this.lastMarker.setMap(null);
        }

        if (this.linesPath != null) {
            this.linesPath.setMap(null);
        }

    }

    addInfoWindowsToMarker(marker: google.maps.Marker, title: string, user: any) {

        let _this = this;
        var contentString = '<div id="content">' +
            '<div id="siteNotice">' + title + '</div>' +
            '<h1 id="firstHeading" class="firstHeading">' + user.userName + '</h1>' +
            '<div id="bodyContent">' +
            '<p><b><i class="fa fa-calendar"></i>&nbsp;Fecha Hora</b>: ' + user.mobilDate + '.</p>' +
            '</b>'
        '</div>' +
            '</div>';

        google.maps.event.addListener(marker, "click", function(e) {
            _this.infoWindow.setContent(contentString);
            _this.infoWindow.open(_this.map, marker);
        });

    }

    addInfoWindowsToUserMarker(marker: google.maps.Marker, user: any) {

        let _this = this;
        var contentString = '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading">' + user.userName + '</h1>' +
            '<div id="bodyContent">' +
            '<p><b><i class="fa fa-calendar"></i>&nbsp;Fecha Hora</b>: ' + user.mobilDate + '.</p>' +
            '<div class="btn btn-default"><i class="fa fa-map"></i>&nbsp;Ver Recorrido</b>'
        '</div>' +
            '</div>';

        google.maps.event.addListener(marker, "click", function(e) {
            _this.infoWindow.setContent(contentString);
            _this.infoWindow.open(_this.map, marker);
        });

    }

    drawRoute(locations: any, loaded: boolean = false, crrntUserName: any): boolean {
        var rst = false;
        this.cleanMap();

        var lines = new Array();
        var lastPoint = null;
        var firstPoint = null;
        var user = null;
        var userLast = null;

        locations.forEach(element => {

            if (element.latitude != undefined && element.betterLocation == "1") {

                lastPoint = { lat: element.latitude, lng: element.longitude };
                userLast = element;

                lines.push(new google.maps.LatLng(element.latitude, element.longitude));

                if (firstPoint == null) {
                    firstPoint = lastPoint;
                    user = element;
                }
                rst = true;
            } else if (firstPoint == null && element.latitude != undefined) {
                lastPoint = { lat: element.latitude, lng: element.longitude };
                firstPoint = lastPoint;
                user = element;
                userLast = element;
            }
        });
        if (user == null || userLast == null) { return rst; }

        this.linesPath = new google.maps.Polyline({
            path: lines,
            strokeColor: '#4398a7',
            strokeOpacity: 1.0,
            strokeWeight: 4,
            map: this.map
        });

        //        this.linesPath.setMap(this.map);

        if (!loaded) {
            this.map.setCenter(lastPoint);
            this.map.setZoom(18);
        }

        this.firstMarker = new google.maps.Marker({
            map: this.map,
            //animation: google.maps.Animation.DROP,
            position: firstPoint,
            icon: this.iconStartUrl,
            draggable: false,
            title: "Inicio"
        });

        this.lastMarker = new google.maps.Marker({
            map: this.map,
            //animation: google.maps.Animation.DROP,
            position: lastPoint,
            icon: this.iconEndUrl,
            draggable: false,
            title: "Fin"
        });

        user.userName = crrntUserName;
        userLast.userName = crrntUserName;
        this.addInfoWindowsToMarker(this.firstMarker, 'Primera localización', user);
        this.addInfoWindowsToMarker(this.lastMarker, 'Ultima localización', userLast);

        return rst;
    }


}