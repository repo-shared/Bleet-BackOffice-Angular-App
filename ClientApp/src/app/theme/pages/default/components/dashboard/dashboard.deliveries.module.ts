
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { Routes, RouterModule } from '@angular/router';
import { DashboardDeliveriesComponent } from "./dashboard.deliveries.component";
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { DxTagBoxModule, DxDataGridModule, DxSwitchModule, DxPopupModule, DxCheckBoxModule, DxTextBoxModule, DxLookupModule, DxDateBoxModule, DxValidatorModule, DxLoadPanelModule, DxSelectBoxComponent, DxSelectBoxModule, DxPieChartModule, DxChartModule } from 'devextreme-angular';
import { PackageService } from '../../../../../_services/package.services';
import { SubsidiaryService } from '../../../../../_services/subsidiary.service'
import { UserService } from '../../../../../_services/user.services'
import { RejectionTypeService } from '../../../../../_services/rejectionType.services';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": DashboardDeliveriesComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,
        DxSwitchModule,
        DxPopupModule,
        DxLookupModule,
        DxDateBoxModule,
        DxSelectBoxModule,
        DxTextBoxModule,
        DxLoadPanelModule,
        DxPieChartModule,
        DxValidatorModule,
        DxCheckBoxModule,
        DxDataGridModule,
        DxChartModule,
        DxTagBoxModule,
        FormsModule
    ], exports: [
        RouterModule
    ], declarations: [
        DashboardDeliveriesComponent
    ],
    providers: [PackageService, SubsidiaryService, UserService, RejectionTypeService, DatePipe],
})
export class DashboardDeliveryModule {
}
