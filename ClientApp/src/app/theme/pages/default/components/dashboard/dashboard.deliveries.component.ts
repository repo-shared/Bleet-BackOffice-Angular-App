import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { SubsidiaryService } from '../../../../../_services/subsidiary.service';
import { Notification } from '../../../../../utils/notification';
import { Helpers } from '../../../../../helpers';
import { LocalStorageUtil } from '../../../../../utils/localstorageutil';
import { UserService } from '../../../../../_services/user.services';
import { PerfilUser } from '../../../../../utils/perfil.user.enum';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { PackageService } from '../../../../../_services/package.services';
import { RejectionTypeService } from '../../../../../_services/rejectionType.services';

@Component({
    selector: "app-admin-dashboardDeliveries",
    templateUrl: "./dashboard.deliveries.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./dashboard.deliveries.component.css']
})
export class DashboardDeliveriesComponent implements OnInit, AfterViewInit {
    //RejectionReason = rr
    //DeliveryMade = dm
    //DeliveryRejected = dr
    //FreeUsers = fu
    //BusyUsers = bu

    public rrDataSource: any = [];
    public dmDataSource: any = [];
    public drDataSource: any = [];
    public fuDataSource: any = [];
    public buDataSource: any = [];
    //Lista de sucursales para cada filtro
    public subSidiaryList: any;
    public rrSubSidiaryList: any;
    public dmSubSidiaryList: any;
    public drSubSidiaryList: any;
    public fuSubSidiaryList: any;
    public buSubSidiaryList: any;
    //Lista de mensajeros para cada filtro
    public rrUserList: any;
    public dmUserList: any;
    public drUserList: any;
    public fuUserList: any;
    public buUserList: any;

    public now: Date = new Date();
    public userLoginData = LocalStorageUtil.getUserLoginData();
    public rejectionTypeList;
    public grossProductData: any;

    constructor(private packageService: PackageService, private subsidiaryService: SubsidiaryService, private userService: UserService, private rjTpSrv: RejectionTypeService, private exportAsService: ExportAsService) {
    }

    //Filters
    public filters: any = {
        startDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()),
        endDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate() + 6),
        idUser: [],
        idSubsidiary: '0'
    };

    public rrFilters: any = {
        startDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()),
        endDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate() + 6),
        idUser: [],
        idSubsidiary: '0',
        idRejectionType: 0
    };

    public dmFilters: any = {
        startDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()),
        endDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate() + 6),
        idUser: [],
        idSubsidiary: '0'
    };

    public drFilters: any = {
        startDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()),
        endDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate() + 6),
        idUser: [],
        idSubsidiary: '0'
    };

    public fuFilters: any = {
        startDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()),
        endDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate() + 6),
        idUser: [],
        idSubsidiary: '0'
    };

    public buFilters: any = {
        startDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate() - 6),
        endDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()),
        idUser: [],
        idSubsidiary: '0'
    };

    ngOnInit() {

        this.subsidiaryService.getSubsidiaries().subscribe(
            result => {
                this.subSidiaryList = result.data;
                this.subSidiaryList.unshift({ id: 0, name: 'Todas' });

                //Fill others
                this.rrSubSidiaryList = this.subSidiaryList;
                this.dmSubSidiaryList = this.subSidiaryList;
                this.drSubSidiaryList = this.subSidiaryList;
                this.fuSubSidiaryList = this.subSidiaryList;
                this.buSubSidiaryList = this.subSidiaryList;

                if (this.userLoginData.rol == PerfilUser.Coordinator) {

                    var item = this.subSidiaryList.filter(s => s.name == this.userLoginData.subsidiaryName)[0];

                    if (item) {
                        this.rrFilters.idSubsidiary = item.id;
                        this.dmFilters.idSubsidiary = item.id;
                        this.drFilters.idSubsidiary = item.id;
                        this.buFilters.idSubsidiary = item.id;
                        this.fuFilters.idSubsidiary = item.id;
                    }
                }

            },
            error => {
                Notification.WebMethodError(error);
            });

        return new Promise((resolve) => {

            this.rjTpSrv.getAll().subscribe(res => {
                this.rejectionTypeList = res.data;

                this.rejectionTypeList.sort(function(a, b) { return b.id - a.id; })

                this.rejectionTypeList.unshift({ id: 0, rejectionName: 'Todos' });
                Helpers.setLoading(false);
                resolve();
            },
                error => {
                    Notification.WebMethodError(error);
                    Helpers.setLoading(false);
                });
        });
    }

    subsidiary_onValueChanged(e) {
        console.log(e.value);

        Helpers.setLoading(true, 'Cargando Usuarios...');
        var method: any;
        if (e.value == 0) {
            method = this.userService.getAll();
        } else {
            method = this.userService.getAllBySubsidiary(e.value);
        }

        method.subscribe(
            result => {
                this.filters.idUser = [];
                var data = new Array();
                result.data.forEach(function(item) {
                    data.push({ id: item.id, fullName: item.fullName, selected: false });
                });

                switch (e.element.id) {
                    case "rrSubsidiary": {
                        this.rrUserList = data;
                        break;
                    }
                    case "dmSubsidiary": {
                        this.dmUserList = data;
                        break;
                    }
                    case "drSubsidiary": {
                        this.drUserList = data;
                        break;
                    }
                    case "fuSubsidiary": {
                        this.fuUserList = data;
                        break;
                    }
                    case "buSubsidiary": {
                        this.buUserList = data;
                        break;
                    }
                }

                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });
    }

    loadRejectedReasonDashboard() {
        Helpers.setLoading(true, 'Generando Graficos...');

        var verify = this.verifyFilter(this.rrFilters, true)
        if (!verify.val) {
            Notification.Error('Favor verificar los parametros. ' + verify.text);
        }
        else {
            this.packageService.getFinalized("", this.rrFilters.startDate, this.rrFilters.endDate).subscribe(result => {

                var rrData = result.data.filter(b => this.drFilters.idUser.includes(b.idUser)
                    && b.statusId == 5
                    && (b.subsidiaryId == this.drFilters.idSubsidiary || 0 == this.drFilters.idSubsidiary));

                if (rrData.length > 0) {
                    this.fillRejectionReasonChart(rrData);
                }
                else {
                    Notification.Error('No se encontraron registros con los parametros brindados.');
                }


            });
        }
        Helpers.setLoading(false);

    }

    fillRejectionReasonChart(_data: any) {

        this.rrDataSource = [];

        var auxSubList = this.groupBy(<any[]>_data, 'rejectionName');

        var count = 0;

        if (auxSubList.length > 0) {
            count = auxSubList.length;
        }

        for (var i = 0; i < count; i++) {
            var _name = auxSubList[i].key;
            var total = auxSubList[i].values.length;
            this.rrDataSource.unshift({ name: _name, val: total });
        }
    }

    loadDeliveriesMadeDashboard() {
        Helpers.setLoading(true, 'Generando Graficos...');
        this.dmDataSource = [];

        var verify = this.verifyFilter(this.dmFilters, false)
        if (!verify.val) {
            Helpers.setLoading(false);
            Notification.Error('Favor verificar los parametros. ' + verify.text);
        }
        else {
            this.packageService.getFinalized("", this.dmFilters.startDate, this.dmFilters.endDate).subscribe(result => {

                //filtrar el resultado          
                var dmData: any = result.data.filter(b => this.dmFilters.idUser.includes(b.idUser)
                    && b.statusId == 4
                    && (b.subsidiaryId == this.dmFilters.idSubsidiary || 0 == this.dmFilters.idSubsidiary));

                if (dmData.length > 0) {
                    //agrupar por subsidiaryId / subsidiaryName
                    var auxSubList = this.groupBy(<any[]>dmData, 'subsidiaryName');

                    var count = 0;

                    if (auxSubList.length > 0) {
                        count = auxSubList.length;
                    }

                    for (var i = 0; i < count; i++) {
                        var _name = auxSubList[i].key;
                        var _valueField = auxSubList[i].values.length;
                        this.dmDataSource.unshift({ name: _name, valueField: _valueField });
                    }
                }
                else {
                    Helpers.setLoading(false);
                    Notification.Error('No se encontraron registros con los parametros brindados.');
                }


            });
        }
        Helpers.setLoading(false);
    }

    loadDeliveriesRejectedDashboard() {

        Helpers.setLoading(true, 'Generando Graficos...');
        this.drDataSource = [];

        var verify = this.verifyFilter(this.drFilters, false)
        if (!verify.val) {
            Notification.Error('Favor verificar los parametros. ' + verify.text);
        }
        else {
            this.packageService.getFinalized("", this.drFilters.startDate, this.drFilters.endDate).subscribe(result => {

                //filtrar el resultado          
                var drData = result.data.filter(b => this.drFilters.idUser.includes(b.idUser)
                    && b.statusId == 5
                    && (b.subsidiaryId == this.drFilters.idSubsidiary || 0 == this.drFilters.idSubsidiary));

                if (drData.length > 0) {
                    //agrupar por subsidiaryId / subsidiaryName
                    var auxSubList = this.groupBy(<any[]>drData, 'subsidiaryName');

                    var count = 0;

                    if (auxSubList.length > 0) {
                        count = auxSubList.length;
                    }

                    for (var i = 0; i < count; i++) {
                        var _name = auxSubList[i].key;
                        var _valueField = auxSubList[i].values.length;
                        this.drDataSource.unshift({ name: _name, valueField: _valueField });
                    }
                }
                else {
                    Notification.Error('No se encontraron registros con los parametros brindados.');
                }


            });
        }
        Helpers.setLoading(false);

    }

    loadBusyUsers() {

        Helpers.setLoading(true, 'Generando Graficos...');
        this.drDataSource = [];

        var verify = this.verifyFilter(this.buFilters, false)
        if (!verify.val) {
            Helpers.setLoading(false);
            Notification.Error('Favor verificar los parametros. ' + verify.text);
        }
        else {
            this.packageService.getUserStatus("", this.buFilters.startDate, this.buFilters.endDate).subscribe(result => {

                var subsidiaryName = this.subSidiaryList.filter(b => b.id == this.fuFilters.idSubsidiary)[0];
                var users = this.fuUserList.filter(x => this.fuFilters.idUser.includes(x.id)).map(x => x.fullName);

                //filtrar el resultado          
                var fuData = result.data.filter(b => b.packagesAssignedCount > 0
                    && users.includes(b.userFullName)
                    && (b.subsidiary == subsidiaryName.name || 0 == this.fuFilters.idSubsidiary));

                if (fuData.length > 0) {
                    //agrupar por subsidiaryId / subsidiaryName
                    var auxSubList = this.groupBy(<any[]>fuData, 'subsidiary');

                    var count = 0;

                    if (auxSubList.length > 0) {
                        count = auxSubList.length;
                    }

                    for (var i = 0; i < count; i++) {
                        var _name = auxSubList[i].key;
                        var _valueField = auxSubList[i].values.length;
                        this.fuDataSource.unshift({ name: _name, valueField: _valueField });
                    }
                }
                else {
                    Helpers.setLoading(false);
                    Notification.Error('No se encontraron registros con los parametros brindados.');
                }


            });
        }
        Helpers.setLoading(false);

    }

    loadFreeUsers() {

        Helpers.setLoading(true, 'Generando Graficos...');
        this.fuDataSource = [];

        var verify = this.verifyFilter(this.fuFilters, false)
        if (!verify.val) {
            Helpers.setLoading(false);
            Notification.Error('Favor verificar los parametros. ' + verify.text);
        }
        else {
            this.packageService.getUserStatus("", this.fuFilters.startDate, this.fuFilters.endDate).subscribe(result => {

                var subsidiaryName = this.subSidiaryList.filter(b => b.id == this.fuFilters.idSubsidiary)[0];
                var users = this.fuUserList.filter(x => this.fuFilters.idUser.includes(x.id)).map(x => x.fullName);

                //filtrar el resultado          
                var fuData = result.data.filter(b => b.packagesAssignedCount < 1
                    && users.includes(b.userFullName)
                    && (b.subsidiary == subsidiaryName.name || 0 == this.fuFilters.idSubsidiary));

                if (fuData.length > 0) {
                    //agrupar por subsidiaryId / subsidiaryName
                    var auxSubList = this.groupBy(<any[]>fuData, 'subsidiary');

                    var count = 0;

                    if (auxSubList.length > 0) {
                        count = auxSubList.length;
                    }

                    for (var i = 0; i < count; i++) {
                        var _name = auxSubList[i].key;
                        var _valueField = auxSubList[i].values.length;
                        this.fuDataSource.unshift({ name: _name, valueField: _valueField });
                    }
                }
                else {
                    Helpers.setLoading(false);
                    Notification.Error('No se encontraron registros con los parametros brindados.');
                }


            });
        }
        Helpers.setLoading(false);

    }

    ngAfterViewInit() {

    }

    verifyFilter(filter: any, applyRejectionType: boolean): any {
        var resultado: boolean = true;
        //var mensaje: Array<string> = [];
        var mensaje: string;

        var _startDate: Date = filter.startDate;
        var _endDate: Date = filter.endDate;

        if (resultado && _startDate > _endDate) {
            //mensaje.push("La fecha inicial no puede ser mayor a la final");
            mensaje = "La fecha inicial no puede ser mayor a la final";
            resultado = false;
        }

        if (resultado && filter.idUser.length == 0 || filter.idUser.count < 0) {
            //mensaje.push("Seleccione sucursal y mensajero");
            mensaje = "Seleccione sucursal y mensajero";
            resultado = false;
        }

        if (resultado && filter.idSubsidiary.length == 0 || filter.idSubsidiary < 0) {
            //mensaje.push("Seleccione una sucursal");
            mensaje = "Seleccione una sucursal";
            resultado = false;
        }

        if (resultado && applyRejectionType && (filter.idRejectionType.length == 0 || filter.idRejectionType < 0)) {
            //mensaje.push("Seleccione un motivo de rechazo");
            mensaje = "Seleccione un motivo de rechazo";
            resultado = false;
        }

        return { val: resultado, text: mensaje };
    }

    groupBy(xs, key) {
        return xs.reduce(function(rv, x) {
            let v = key instanceof Function ? key(x) : x[key]; let el = rv.find((r) => r && r.key === v);
            if (el) {
                el.values.push(x);
            }
            else {
                rv.push({ key: v, values: [x] });
            }
            return rv;
        }, []);
    }

}