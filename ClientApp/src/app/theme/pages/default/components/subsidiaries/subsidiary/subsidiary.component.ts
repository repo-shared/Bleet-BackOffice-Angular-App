import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { ActivatedRoute, Router } from '@angular/router';
import { } from 'googlemaps';
import { SubsidiaryService } from '../../../../../../_services/subsidiary.service';
import { RegionService } from '../../../../../../_services/region.service';
import { SubsidiaryGeoPointService } from '../../../../../../_services/subsidiarygeopoint.service';
import { Notification } from '../../../../../../utils/notification';

@Component({
    selector: 'app-subsidiary',
    templateUrl: "./subsidiary.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class SubsidiaryComponent implements OnInit {

    @ViewChild('gmap') mapElement: ElementRef;
    map: google.maps.Map;

    public model = {
        id: '',
        name: '',
        address: '',
        phoneNumber: '',
        idCompany: '',
        latitude: '',
        longitude: '',
        hasGeofence: '',
        geofenceRadius: '',
        active: true
    };
    public regiones: any[];
    private interval: any;
    public SubsidiaryList: any;
    public SubGeoPointList: any;
    public subsidiaryService: SubsidiaryService;
    public regionService: RegionService;
    public subsidiaryGeoPointService: SubsidiaryGeoPointService;

    constructor(subsidiaryService: SubsidiaryService,
        regionService: RegionService,
        subsidiaryGeoPointService: SubsidiaryGeoPointService,
        private route: ActivatedRoute,
        private router: Router) {
        this.subsidiaryService = subsidiaryService;
        this.regionService = regionService;
        this.subsidiaryGeoPointService = subsidiaryGeoPointService;
    }

    async ngOnInit() {
        await this.getRegions();
        await this.getSubGeoPoints();
        this.getSubsidiaries();
    }

    ngOnDestroy() {
        if (this.interval) {
            clearTimeout(this.interval);
        }
    }

    getSubsidiaries() {
        Helpers.setLoading(true, 'Cargando Sucursales...');
        this.subsidiaryService.getSubsidiaries().subscribe(
            result => {
                this.SubsidiaryList = result.data;
                this.SubsidiaryList.sort(function(a, b) { return b.name - a.name; })


                var auxlist = <any[]>this.SubsidiaryList;
                var auxRegion_list = <any[]>this.regiones;
                var auxGeoPoint_list = <any[]>this.SubGeoPointList;

                auxlist.forEach(sub => {
                    if (sub.idRegion != null || sub.idRegion != undefined) {
                        auxRegion_list.forEach(region => {
                            if (sub.idRegion == region.id) {
                                sub["regionName"] = region.regionName;
                                return;
                            }
                        });
                    }
                    if (auxGeoPoint_list != undefined) {
                        auxGeoPoint_list.forEach(geo => {
                            if (sub.id == geo.idSubsidiary) {
                                sub["hasPoligonalGeo"] = true;
                                return;
                            }
                        });
                    }
                    else {
                        sub["hasPoligonalGeo"] = false;
                    }

                    if (sub["hasPoligonalGeo"] == undefined) {
                        sub["hasPoligonalGeo"] = false;
                    }

                });

                this.SubsidiaryList = auxlist;

                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });
    }

    MoveToModifier(SubID: string) {
        this.router.navigateByUrl('/subsidiaries/subsidiary/data/' + SubID);
    }

    async getRegions() {
        let result = await this.regionService.getRegions().toPromise();

        if (result.data != undefined && result.data.length > 0) {
            this.regiones = result.data;
            this.regiones.sort(function(a, b) { return b.name - a.name; });
        }
    }

    async getSubGeoPoints() {
        let result = await this.subsidiaryGeoPointService.getSubsidiariesGeoPoints().toPromise();

        if (result.data != undefined && result.data.length > 0) {
            this.SubGeoPointList = result.data;
        }
    }
}
