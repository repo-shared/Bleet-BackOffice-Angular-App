import { } from 'googlemaps';
import { Notification } from '../../../../../../utils/notification';
import { SubsidiaryService } from '../../../../../../_services/subsidiary.service';
import { SubsidiaryGeoPointService } from '../../../../../../_services/subsidiarygeopoint.service';

export class DataComponentHelper {

    private map: google.maps.Map;

    private markers = [];
    private Marker: google.maps.Marker;
    private MarkerAux: google.maps.Marker = new google.maps.Marker();
    private Circle: google.maps.Circle;
    private CircleAux: google.maps.Circle = new google.maps.Circle();
    public StartMarkerPosition: any;
    private infoWindow: google.maps.InfoWindow;
    private color_actualiza: string = '#299781';
    private greenIcon = '/assets/app/media/img/subsidiary/green-dot.png';
    private redIcon = '/assets/app/media/img/subsidiary/red-dot.png';
    private blueIcon = '/assets/app/media/img/subsidiary/blue-dot.png';
    private all_OverLays: any[] = new Array();
    private all_PolygonOverLays: any[] = new Array();
    private time: number = 5000;
    private drawingManager: google.maps.drawing.DrawingManager;
    private Polygon: google.maps.Polygon;
    private PolygonAux: google.maps.Polygon = new google.maps.Polygon();
    private polyOptions = {
        fillColor: '#f8f9fa',
        fillOpacity: 0.6,
        strokeColor: '#6f42c1',
        strokeWeight: 2,
        editable: true,
        draggable: true
    };
    private disabled_polyOptions = {
        fillColor: '#f8f9fa',
        fillOpacity: 0.5,
        strokeColor: '#BBD8E9',
        strokeWeight: 3,
        editable: false,
        draggable: false
    };
    private EdgeTolerance: number = 0.000919;
    private isOnDrag: boolean = false;
    private geocoder: google.maps.Geocoder;

    constructor(map: google.maps.Map, subsidiaryService: SubsidiaryService, subsidiaryGeoPointService: SubsidiaryGeoPointService) {
        this.map = map;
        this.infoWindow = new google.maps.InfoWindow();
        subsidiaryService.Circle$.subscribe((data) => {
            this.CircleAux = data;
        });
        subsidiaryService.Marker$.subscribe((data) => {
            this.MarkerAux = data;
        });
        subsidiaryService.StartPosition$.subscribe((data) => {
            this.StartMarkerPosition = data;
        });
    }

    cleanMap() {
        this.markers.forEach(function(marker) {
            marker.setMap(null);
        });
        this.markers = [];
    }

    addInfoWindowsToMarker(marker: google.maps.Marker, title: string, subsidiary: any) {

        let _this = this;
        var contentString = '<div id="content">' +
            '<div id="siteNotice">' + title + '</div>' +
            '<h1 id="firstHeading" class="firstHeading">' + subsidiary.name + '</h1>' +
            '<div id="bodyContent">' +
            '<p><b><i class="fa fa-calendar"></i>&nbsp;Fecha Hora</b>: ' + new Date().toDateString() + '.</p>' +
            '</b>'
        '</div>' +
            '</div>';

        google.maps.event.addListener(marker, "click", function(e) {
            _this.infoWindow.setContent(contentString);
            _this.infoWindow.open(_this.map, marker);
        });

    }

    drawSubsidiaries(subsidiaries: any, Radio: string, subsidiary: any, subsidiaryService: SubsidiaryService, subid: number, subsidiaryGEoPointList: any) {
        this.cleanMap();
        var self = this;
        if (subsidiaries == undefined)
            return;
        var auxlist = <any[]>subsidiaries;

        auxlist.forEach(sub => {
            if (sub.hasGeofence) {
                var subCircle = new google.maps.Circle({
                    strokeColor: this.color_actualiza,
                    strokeOpacity: 0.3,
                    strokeWeight: 2,
                    fillColor: this.color_actualiza,
                    fillOpacity: 0.2,
                    map: this.map,
                    center: new google.maps.LatLng(parseFloat(sub.latitude), parseFloat(sub.longitude)),
                    radius: parseFloat(Radio)
                });
            }

            var Marker = new google.maps.Marker({
                map: this.map,
                position: new google.maps.LatLng(parseFloat(sub.latitude), parseFloat(sub.longitude)),
                draggable: false,
                icon: this.blueIcon,
                title: "Localización de sucursal"
            });

            if (subsidiary.id == sub.id) {
                this.Marker = Marker;
                this.Marker.setDraggable(true);
                this.Marker.setIcon(this.redIcon);
                var multi = 1, validCircle = false;
                subsidiaryService.Marker(this.Marker);
                subsidiaryService.MarkerPosition(Marker.getPosition());
                if (subCircle != undefined) {
                    subsidiaryService.Circle(subCircle);
                    this.Circle = subCircle;
                    this.Circle.setOptions({
                        strokeColor: '#6f42c1',
                        strokeOpacity: 0.3,
                        strokeWeight: 2,
                        fillColor: '#6f42c1',
                        fillOpacity: 0.2,
                        map: this.map,
                        center: new google.maps.LatLng(parseFloat(sub.latitude), parseFloat(sub.longitude)),
                        radius: parseFloat(Radio)
                    });
                    this.Marker.bindTo("position", this.Circle, "center");
                    multi = 2;
                    validCircle = true;
                }
                self.addOnDragEvents(self.Marker, subsidiaryService, subsidiaries, Radio, subid, multi, validCircle, subsidiaryGEoPointList, self);
            }

            this.addInfoWindowsToMarker(Marker, Marker.getTitle(), sub);
        });
    }

    setMarkOnClick(map: google.maps.Map, radio: string, subsidiaryList: any, subsidiaryService: SubsidiaryService, subid: number, subsidiaryGEoPointList: any) {
        var self = this;
        google.maps.event.addListener(map, 'click', function(e) {
            self.doOnClickSingleMarkerWork(map, radio, self, subid, e.latLng, subsidiaryList, subsidiaryService, subsidiaryGEoPointList);
        });
    }

    setCircleOnClick(map: google.maps.Map, showCircle: boolean, radio: string, subsidiaryList: any, subsidiaryService: SubsidiaryService, subid: number, subsidiaryGEoPointList: any) {
        if (showCircle) {
            var self = this;
            google.maps.event.clearListeners(map, 'click');
            if (this.MarkerAux != undefined) {
                google.maps.event.clearListeners(this.MarkerAux, 'dragend');
                google.maps.event.clearListeners(this.MarkerAux, 'dragstart');
            }

            google.maps.event.addListener(map, "click", function(e) {
                self.doOnClickCircleWork(map, radio, self, subid, e.latLng, subsidiaryList, subsidiaryService, subsidiaryGEoPointList);
            });
        }
        else {
            google.maps.event.clearListeners(map, 'click');
            if (this.MarkerAux != undefined) {
                google.maps.event.clearListeners(this.MarkerAux, 'dragend');
                google.maps.event.clearListeners(this.MarkerAux, 'dragstart');
            }
            this.setMarkOnClick(map, radio, subsidiaryList, subsidiaryService, subid, subsidiaryGEoPointList);
        }
    }

    drawCircle(map: google.maps.Map, showCircle: boolean, radio: string, subsidiaryList: any, subsidiaryService: SubsidiaryService, subid: number, subsidiaryGEoPointList: any) {
        var self = this;
        if (showCircle) {
            if (this.CircleAux != undefined) {
                this.Circle = this.CircleAux;
                this.Marker = this.MarkerAux;

                if (this.Circle.getCenter() == undefined) {
                    this.Circle = new google.maps.Circle({
                        fillColor: '#BBD8E9',
                        fillOpacity: 0.5,
                        radius: parseFloat(radio),
                        strokeColor: '#BBD8E9',
                        strokeOpacity: 0.7,
                        strokeWeight: 3
                    });
                }
                var valPosition = this.ValidatePosition(subsidiaryList, this.Circle, radio, subid, this.Marker.getPosition(), 2, true, subsidiaryGEoPointList, self);
                if (valPosition == true) {
                    map.panTo(this.Marker.getPosition());
                    this.Circle.setCenter(this.Marker.getPosition());
                    this.Circle.setMap(map);
                    this.Marker.bindTo("position", this.Circle, "center");

                    if (this.Marker != undefined) {
                        google.maps.event.clearListeners(this.Marker, 'dragend');
                        google.maps.event.clearListeners(this.Marker, 'dragstart');
                    }
                    subsidiaryService.Circle(this.Circle);
                    subsidiaryService.Marker(this.MarkerAux);

                    this.addOnDragEvents(this.Marker, subsidiaryService, subsidiaryList, radio, subid, 2, true, subsidiaryGEoPointList, self);
                    return true;
                } else { return false; }
            }
        }
        else {
            if (this.CircleAux != undefined) {
                this.CircleAux.setMap(null);
                this.Marker = this.MarkerAux;

                if (this.Marker != undefined) {
                    google.maps.event.clearListeners(this.Marker, 'dragend');
                    google.maps.event.clearListeners(this.Marker, 'dragstart');
                }

                this.addOnDragEvents(this.Marker, subsidiaryService, subsidiaryList, radio, subid, 1, false, subsidiaryGEoPointList, self);
            }
        }
    }

    addOnDragEvents(Marker: google.maps.Marker, subsidiaryService: SubsidiaryService, subsidiaryList: any, radio: string, subid: number, multiplier: number, validCircle: boolean, subsidiaryGEoPointList: any, Helper: any) {
        var self_ = this;
        google.maps.event.addListener(Marker, "dragstart", function(e) {
            subsidiaryService.StartPosition(e.latLng);
        });

        google.maps.event.addListener(Marker, "dragend", function(e) {
            var valPosition = self_.ValidatePosition(subsidiaryList, self_.CircleAux, radio, subid, e.latLng, multiplier, validCircle, subsidiaryGEoPointList, Helper);

            if (valPosition == true) {
                subsidiaryService.MarkerPosition(e.latLng);
                subsidiaryService.Marker(this);
            } else {
                this.setPosition(self_.StartMarkerPosition);
            }
        });

    }

    ValidatePosition(subsidiaryList: any, Circle: any, radio: string, subid: any, ActualPOS: any, Multiplier: number, valCircle: boolean, subsidiaryGEoPointList: any, self: any) {

        var auxlist = <any[]>subsidiaryList;
        var customMessage = "";
        if (subsidiaryList == undefined || auxlist.length == 0) return true;
        var isInRestrictedZone: { name: string, isRestriced: boolean, validateProces: number }[] = new Array();
        auxlist.forEach(sub => {
            if (sub.hasGeofence && sub.id != subid) {
                var cityCircle = new google.maps.Circle({
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.35,
                    strokeWeight: 2,
                    fillColor: '#FF0000',
                    fillOpacity: 0.35,
                    center: new google.maps.LatLng(parseFloat(sub.latitude), parseFloat(sub.longitude)),
                    radius: parseFloat(radio),
                    draggable: true
                });

                var check = google.maps.geometry.spherical.computeDistanceBetween(ActualPOS, cityCircle.getCenter()) <= (parseFloat(radio) * Multiplier);
                isInRestrictedZone.push({ name: sub.name, isRestriced: check, validateProces: 1 });
            }
            else if (sub.hasGeofence == false && (subid != 0 || subid == undefined) && sub.id != subid && Circle != undefined) {
                var check = google.maps.geometry.spherical.computeDistanceBetween(ActualPOS, new google.maps.LatLng(parseFloat(sub.latitude), parseFloat(sub.longitude))) <= (parseFloat(radio) * Multiplier);
                isInRestrictedZone.push({ name: sub.name, isRestriced: check, validateProces: 2 });
            }
            else if (sub.hasGeofence == false && (subid != 0 || subid == undefined) && sub.id != subid) {
                var check = google.maps.geometry.spherical.computeDistanceBetween(ActualPOS, new google.maps.LatLng(parseFloat(sub.latitude), parseFloat(sub.longitude))) <= (5);
                isInRestrictedZone.push({ name: sub.name, isRestriced: check, validateProces: 3 });
            }
            else if ((subid == 0 || subid == undefined) && valCircle == true) {
                var check = google.maps.geometry.spherical.computeDistanceBetween(ActualPOS, new google.maps.LatLng(parseFloat(sub.latitude), parseFloat(sub.longitude))) <= (parseFloat(radio) * Multiplier);
                isInRestrictedZone.push({ name: sub.name, isRestriced: check, validateProces: 4 });
            }
            else if ((subid == 0 || subid == undefined) && valCircle == false) {
                var check = google.maps.geometry.spherical.computeDistanceBetween(ActualPOS, new google.maps.LatLng(parseFloat(sub.latitude), parseFloat(sub.longitude))) <= (5);
                isInRestrictedZone.push({ name: sub.name, isRestriced: check, validateProces: 5 });
            }

            if (subsidiaryGEoPointList != undefined && sub.id != subid) {
                var auxlist_ = self.groupBy(<any[]>subsidiaryGEoPointList, 'idSubsidiary');

                auxlist_.forEach(item => {
                    if (item.key != subid) {
                        var pol = new google.maps.Polygon();

                        pol.setOptions(self.polyOptions);

                        var paths = new Array();
                        item.values.forEach(val => {
                            paths.push(new google.maps.LatLng(parseFloat(val.latitude), parseFloat(val.longitude)));
                        });
                        pol.setPath(paths);

                        if (sub.hasGeofence == true) {

                            if (google.maps.geometry.poly.containsLocation(ActualPOS, pol) == true) {
                                var name = "";

                                auxlist.forEach(subitem => { if (subitem.id == item.key) name = subitem.name });
                                if (isInRestrictedZone.findIndex(zone => zone.name == name && zone.isRestriced == true) < 0) {
                                    isInRestrictedZone.push({ name: name, isRestriced: true, validateProces: 2 });
                                }
                            }
                            else if (self.isPolygoninCircle(ActualPOS, sub.geofenceRadius, pol.getPath(), self) == true) {
                                var name = "";

                                auxlist.forEach(subitem => { if (subitem.id == item.key) name = subitem.name });
                                if (isInRestrictedZone.findIndex(zone => zone.name == name && zone.isRestriced == true) < 0) {
                                    isInRestrictedZone.push({ name: name, isRestriced: true, validateProces: 2 });
                                }
                            }
                        }
                    }
                    else if (item.key == subid) {
                        var pol = new google.maps.Polygon();
                        pol.setOptions(self.polyOptions);

                        var paths = new Array();
                        item.values.forEach(val => {
                            paths.push(new google.maps.LatLng(parseFloat(val.latitude), parseFloat(val.longitude)));
                        });
                        pol.setPath(paths);

                        if (google.maps.geometry.poly.containsLocation(ActualPOS, pol) == false) {
                            customMessage = 'La ubicación de la sucursal debe estar dentro del área poligonal';
                            var name = "";

                            auxlist.forEach(subitem => { if (subitem.id == item.key) name = subitem.name });
                            if (isInRestrictedZone.findIndex(zone => zone.name == name && zone.isRestriced == true) < 0) {
                                isInRestrictedZone.push({ name: name, isRestriced: true, validateProces: 2 });
                            }
                        }
                    }
                }
                );
            }
            if (self.all_OverLays.length > 0) {
                var pol = new google.maps.Polygon();
                pol.setPath(self.all_OverLays[0]);

                if (google.maps.geometry.poly.containsLocation(ActualPOS, pol) == false) {
                    customMessage = 'La ubicación de la sucursal debe estar dentro del área poligonal';
                    isInRestrictedZone.push({ name: name, isRestriced: true, validateProces: 2 });
                }
            }
        });


        if (isInRestrictedZone.findIndex(zone => zone.isRestriced == true) > -1) {
            var localidades = '';
            isInRestrictedZone.forEach(rzone => {
                if (rzone.isRestriced == true) {
                    localidades += ' ' + rzone.name + ','
                }
            });
            if (customMessage == "") {
                Notification.CustomError('No se puede establecer la localidad indicada, debido a que coincide con las siguientes sucursales:' + localidades.replace(/.$/, '.'), this.time);
            } else {
                Notification.CustomError(customMessage, this.time);
            }

            return false;
        }
        else {
            return true;
        }
    }

    LookAddressAtMap(map: google.maps.Map, address: string, hasGeoFence: boolean, radio: string, subid: any, subsidiaryList: any, subsidiaryService: SubsidiaryService, subsidiaryGEoPointList: any) {
        this.geocoder = new google.maps.Geocoder();
        var self = this;
        this.geocoder.geocode({ 'address': address }, function(results, status) {
            if (status.toString() == 'OK') {
                map.setCenter(results[0].geometry.location);
                if (hasGeoFence == true) {
                    self.doOnClickCircleWork(map, radio, self, subid, results[0].geometry.location, subsidiaryList, subsidiaryService, subsidiaryGEoPointList);
                }
                else {
                    self.doOnClickSingleMarkerWork(map, radio, self, subid, results[0].geometry.location, subsidiaryList, subsidiaryService, subsidiaryGEoPointList);
                }
            }
        });
    }

    doOnClickCircleWork(map: google.maps.Map, radio: string, self: any, subid: any, ActualPOS: any, subsidiaryList: any, subsidiaryService: SubsidiaryService, subsidiaryGEoPointList: any) {
        if (self.Marker != undefined) {
            self.Marker.setMap(null);
        }
        if (self.Circle != undefined) self.Circle.setMap(null);
        self.Circle = new google.maps.Circle({
            fillColor: '#6f42c1',
            fillOpacity: 0.5,
            radius: parseFloat(radio),
            strokeColor: '#6f42c1',
            strokeOpacity: 0.7,
            strokeWeight: 3,
            center: ActualPOS
        });

        var valPosition = self.ValidatePosition(subsidiaryList, self.Circle, radio, subid, ActualPOS, 2, true, subsidiaryGEoPointList, self);
        if (valPosition == true) {
            self.Marker = new google.maps.Marker({
                position: ActualPOS,
                map: map,
                icon: self.redIcon,
                draggable: true
            });
            map.panTo(ActualPOS);
            self.Circle.setMap(map);
            self.Marker.bindTo("position", self.Circle, "center");
            subsidiaryService.Circle(self.Circle);
            subsidiaryService.Marker(self.Marker);
            subsidiaryService.StartPosition(ActualPOS);
            subsidiaryService.MarkerPosition(self.Marker.getPosition());
            self.addOnDragEvents(self.Marker, subsidiaryService, subsidiaryList, radio, subid, 2, true, subsidiaryGEoPointList, self);
        }
        else {
            if (self.StartMarkerPosition != undefined) {
                self.Marker = new google.maps.Marker({
                    position: self.StartMarkerPosition,
                    map: map,
                    icon: self.redIcon,
                    draggable: true
                });
                map.panTo(self.StartMarkerPosition);
                self.Circle.setCenter(self.StartMarkerPosition);
                self.Circle.setMap(map);
                self.Marker.bindTo("position", self.Circle, "center");
            }
        }
    }

    doOnClickSingleMarkerWork(map: google.maps.Map, radio: string, self: any, subid: any, ActualPOS: any, subsidiaryList: any, subsidiaryService: SubsidiaryService, subsidiaryGEoPointList: any) {
        if (self.Marker != undefined) self.Marker.setMap(null);
        if (self.Circle != undefined) self.Circle.setMap(null);

        var validatePosition = self.ValidatePosition(subsidiaryList, self.Circle, radio, subid, ActualPOS, 1, false, subsidiaryGEoPointList, self);
        if (validatePosition == true) {

            self.Marker = new google.maps.Marker({
                position: ActualPOS,
                map: map,
                icon: self.redIcon,
                draggable: true
            });
            map.panTo(ActualPOS);
            subsidiaryService.Marker(self.Marker);
            subsidiaryService.StartPosition(ActualPOS);
            subsidiaryService.MarkerPosition(self.Marker.getPosition());
            self.addOnDragEvents(self.Marker, subsidiaryService, subsidiaryList, radio, subid, 1, false, subsidiaryGEoPointList, self);
        }
        else {
            if (self.StartMarkerPosition != undefined) {
                self.Marker = new google.maps.Marker({
                    position: self.StartMarkerPosition,
                    map: map,
                    icon: self.redIcon,
                    draggable: true
                });
                map.panTo(self.StartMarkerPosition);
            }
        }
    }

    inBoundedBox(latlong1: google.maps.LatLng, latlong2: google.maps.LatLng, latlong3: google.maps.LatLng) {
        var betweenLats;
        var betweenLons;

        if (latlong1.lat() < latlong2.lat())
            betweenLats = (latlong1.lat() <= latlong3.lat() &&
                latlong2.lat() >= latlong3.lat());
        else
            betweenLats = (latlong1.lat() >= latlong3.lat() &&
                latlong2.lat() <= latlong3.lat());

        if (latlong1.lng() < latlong2.lng())
            betweenLons = (latlong1.lng() <= latlong3.lng() &&
                latlong2.lng() >= latlong3.lng());
        else
            betweenLons = (latlong1.lng() >= latlong3.lng() &&
                latlong2.lng() <= latlong3.lng());

        return (betweenLats && betweenLons);
    }

    SimplePolylineIntersection(latlong1: google.maps.LatLng, latlong2: google.maps.LatLng, latlong3: google.maps.LatLng, latlong4: google.maps.LatLng) {
        //Line segment 1 (p1, p2)
        var A1 = latlong2.lat() - latlong1.lat();
        var B1 = latlong1.lng() - latlong2.lng();
        var C1 = A1 * latlong1.lng() + B1 * latlong1.lat();

        //Line segment 2 (p3,  p4)
        var A2 = latlong4.lat() - latlong3.lat();
        var B2 = latlong3.lng() - latlong4.lng();
        var C2 = A2 * latlong3.lng() + B2 * latlong3.lat();

        var determinate = A1 * B2 - A2 * B1;

        var intersection;
        if (determinate != 0) {
            var x = (B2 * C1 - B1 * C2) / determinate;
            var y = (A1 * C2 - A2 * C1) / determinate;

            var intersect = new google.maps.LatLng(y, x);

            if (this.inBoundedBox(latlong1, latlong2, intersect) &&
                this.inBoundedBox(latlong3, latlong4, intersect))
                intersection = intersect;
            else
                intersection = null;
        }
        else //lines are parrallel
            intersection = null;

        return intersection;
    }

    ArePolygonsOverlapped(poly1, poly2?) {
        if (poly2 == undefined) {
            if (poly1.getLength() >= 3) {
                for (var i = 0; i < poly1.getLength() - 1; i++) {
                    for (var k = 0; k < poly1.getLength() - 1; k++) {
                        if (i == k || (i + 1) == k || (k + 1) == i) continue;

                        if (this.SimplePolylineIntersection(poly1.getAt(i), poly1.getAt(i + 1), poly1.getAt(k), poly1.getAt(k + 1)) != null) {
                            return true;
                        }
                    }
                }
            }
        }
        else {
            if (poly1.getLength() >= 3 && poly2.getLength() >= 3) {
                for (var i = 0; i < poly1.getLength() - 1; i++) {
                    for (var k = 0; k < poly2.getLength() - 1; k++) {
                        if (this.SimplePolylineIntersection(poly1.getAt(i), poly1.getAt(i + 1), poly2.getAt(k), poly2.getAt(k + 1)) != null) {
                            return true;
                        }

                    }
                }
            }
        }

        return false;
    }

    setDrawingMode(map: google.maps.Map, showDrawingManager: boolean, subGeoPointList: any, subgeopointService: SubsidiaryGeoPointService, subsidiaryID: number, subsidiaryList: any) {

        var AuxVariable;
        if (showDrawingManager == false) {
            if (subGeoPointList == undefined) {
                showDrawingManager = true;
            } else {
                var auxlist = <any[]>subGeoPointList;

                auxlist.forEach(sub => {
                    if (sub.idSubsidiary == subsidiaryID) {
                        AuxVariable = 1;
                        return;
                    }
                });
            }
            if (AuxVariable == undefined) showDrawingManager = true;
        }
        var self = this;
        if (subGeoPointList != undefined) {
            this.drawPolygons(map, subGeoPointList, subsidiaryID, self);
        }

        if (showDrawingManager == true) {
            this.drawingManager = new google.maps.drawing.DrawingManager({
                drawingControlOptions: {
                    drawingModes: [
                        google.maps.drawing.OverlayType.POLYGON
                    ],
                    position: google.maps.ControlPosition.TOP_CENTER
                },
                polygonOptions: this.polyOptions,
                map: map
            });

            google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function(e) {
                if (self.ValidatePolygonPosition(subsidiaryList, subGeoPointList, e.overlay, subsidiaryID, self) == true) {
                    //self.all_OverLays.push(e.overlay);

                    var path = new Array();

                    for (var i = 0; i < e.overlay.getPath().length; i++) {
                        path.push(e.overlay.getPath().getAt(i));
                    }
                    self.all_OverLays.push(path)

                    self.drawingManager.setDrawingMode(null);
                    subgeopointService.Polygon(e.overlay);

                    self.addOnDragEventPolygon(map, e.overlay, self, subsidiaryList, subGeoPointList, subsidiaryID, subgeopointService);

                    if (self.all_PolygonOverLays.length > 0) {
                        self.all_PolygonOverLays.forEach(pol => {
                            pol.overlay.setMap(null);
                        })
                    }

                    self.all_PolygonOverLays.push(e);
                } else {
                    e.overlay.setMap(null);
                    self.drawingManager.setDrawingMode(null);
                }
            });

            google.maps.event.addListener(this.drawingManager, 'drawingmode_changed', function(e) {
                if (self.drawingManager.getDrawingMode() == google.maps.drawing.OverlayType.POLYGON) {
                    if (self.Marker == undefined) {
                        Notification.CustomError("Debe indicar la localización de la sucursal", this.time);
                        self.drawingManager.setDrawingMode(null);
                        return;
                    }
                }
            });

        }
        else {
            this.drawingManager = new google.maps.drawing.DrawingManager({
                drawingControlOptions: {
                    drawingModes: [
                        null
                    ],
                    position: google.maps.ControlPosition.TOP_CENTER
                },
                polygonOptions: {
                    editable: true,
                    draggable: true
                },
                map: map
            });

            subgeopointService.Polygon(this.Polygon);

            this.addOnDragEventPolygon(map, this.Polygon, self, subsidiaryList, subGeoPointList, subsidiaryID, subgeopointService);

        }

    }

    ValidatePolygonPosition(subsidiaryList: any, subsidiaryGEoPointList: any, polygon: any, subid: any, self: any) {
        if (polygon.getPath().getLength() < 3) {
            Notification.CustomError('Debe establecer al menos 3 vertices para crear un poligono', self.time);
            return false;
        }

        if (google.maps.geometry.poly.containsLocation(self.Marker.getPosition(), polygon) == false) {
            Notification.CustomError('El poligono debe contener la ubicación de la sucursal', self.time);
            return false;
        }
        var isInRestrictedZone: { name: string, isRestriced: boolean, validateProces: number }[] = new Array();


        if (subsidiaryList != undefined) {
            var auxlist = <any[]>subsidiaryList;

            auxlist.forEach(sub => {
                if (sub.id != subid) {
                    if (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(parseFloat(sub.latitude), parseFloat(sub.longitude)), polygon) == true) {
                        isInRestrictedZone.push({ name: sub.name, isRestriced: true, validateProces: 1 });
                    }
                    else if (sub.hasGeofence == true) {
                        if (self.isPolygoninCircle(new google.maps.LatLng(parseFloat(sub.latitude), parseFloat(sub.longitude)), sub.geofenceRadius, polygon.getPath(), self) == true) {
                            isInRestrictedZone.push({ name: sub.name, isRestriced: true, validateProces: 2 });
                        }
                    }
                }
            });
        }

        if (subsidiaryGEoPointList != undefined) {
            var auxlist_ = self.groupBy(<any[]>subsidiaryGEoPointList, 'idSubsidiary');

            auxlist_.forEach(item => {
                if (item.key != subid) {
                    var pol = new google.maps.Polygon();

                    pol.setOptions(self.polyOptions);

                    var paths = new Array();
                    item.values.forEach(val => {
                        paths.push(new google.maps.LatLng(parseFloat(val.latitude), parseFloat(val.longitude)));
                    });
                    pol.setPath(paths);
                    if (self.ArePolygonsOverlapped(polygon.getPath(), pol.getPath())) {
                        var name = "";
                        auxlist.forEach(sub => { if (sub.id == item.key) { name = sub.name; return; } });
                        isInRestrictedZone.push({ name: name, isRestriced: true, validateProces: 3 });
                    }
                }
            });
        }

        if (self.ArePolygonsOverlapped(polygon.getPath())) {
            Notification.CustomError('No es posible crear un poligono con intersecciones en el mismo', this.time);
            return false;
        }

        if (isInRestrictedZone.findIndex(zone => zone.isRestriced == true) > -1) {
            var localidades = '';
            isInRestrictedZone.forEach(rzone => {
                if (rzone.isRestriced == true) {
                    localidades += ' ' + rzone.name + ',';
                }
            });

            Notification.CustomError('No se puede establecer el poligono, debido a que coincide con las siguientes sucursales:' + localidades.replace(/.$/, '.'), this.time);
            return false;
        }
        else {
            return true;
        }
    }

    isPolygoninCircle(CirclePos: any, radio: any, polygon: any, self: any) {
        for (var i = 0; i < polygon.getLength(); i++) {
            if (google.maps.geometry.spherical.computeDistanceBetween(polygon.getAt(i), CirclePos) <= (parseFloat(radio)) == true)
                return true;
        }

        var Poly = new google.maps.Polyline();
        Poly.setPath(polygon);

        if (google.maps.geometry.poly.isLocationOnEdge(CirclePos, Poly, self.EdgeTolerance) == true) {
            return true;
        }

        return false;
    }

    drawPolygons(map: google.maps.Map, subsidiaryGEoPointList: any, subid: any, self: any) {
        var auxlist = self.groupBy(<any[]>subsidiaryGEoPointList, 'idSubsidiary');

        auxlist.forEach(item => {
            if (item.key == subid) {
                var pol = new google.maps.Polygon();

                pol.setOptions(self.polyOptions);
                pol.setMap(map);

                var paths = new Array();
                item.values.forEach(val => {
                    paths.push(new google.maps.LatLng(parseFloat(val.latitude), parseFloat(val.longitude)));
                });
                pol.setPath(paths);
                self.Polygon = pol;
                self.all_OverLays.push(paths);
            } else {
                var pol = new google.maps.Polygon();

                pol.setOptions(self.disabled_polyOptions);
                pol.setMap(map);

                var paths = new Array();
                item.values.forEach(val => {
                    paths.push(new google.maps.LatLng(parseFloat(val.latitude), parseFloat(val.longitude)));
                });
                pol.setPath(paths);
            }
        });
    }

    groupBy(xs, key) {
        return xs.reduce(function(rv, x) {
            let v = key instanceof Function ? key(x) : x[key]; let el = rv.find((r) => r && r.key === v);
            if (el) {
                el.values.push(x);
            }
            else {
                rv.push({ key: v, values: [x] });
            }
            return rv;
        }, []);
    }

    addOnDragEventPolygon(map: google.maps.Map, Polygon: google.maps.Polygon, self: any, subsidiaryList: any, subGeoPointList: any, subsidiaryID: any, subgeopointService: any) {

        google.maps.event.addListener(Polygon, 'dragstart', function() {
            self.isOnDrag = true;
        });

        google.maps.event.addListener(Polygon, 'dragend', function() {
            if (self.ValidatePolygonPosition(subsidiaryList, subGeoPointList, Polygon, subsidiaryID, self) == true) {
                var path = new Array();

                for (var i = 0; i < this.getPath().length; i++) {
                    path.push(this.getPath().getAt(i));
                }
                self.all_OverLays.unshift(path);
                var poly = new google.maps.Polygon();
                poly.setPath(this.getPath());
                subgeopointService.Polygon(poly);
            } else {
                Polygon.setMap(null);
                var auxPolygon = new google.maps.Polygon();
                auxPolygon.setOptions(self.polyOptions);
                auxPolygon.setPath(self.all_OverLays[0]);

                Polygon = auxPolygon;

                Polygon.setMap(map);
                self.addOnDragEventPolygon(map, Polygon, self, subsidiaryList, subGeoPointList, subsidiaryID, subgeopointService);
            }
            self.isOnDrag = false;
        });

        google.maps.event.addListener(Polygon.getPath(), 'insert_at', function() {
            var pol_aux = new google.maps.Polygon();
            var path_ = new Array();
            for (var i = 0; i < this.length; i++) {
                path_.push(this.getAt(i));
            }
            pol_aux.setPath(path_);

            if (self.ValidatePolygonPosition(subsidiaryList, subGeoPointList, pol_aux, subsidiaryID, self) == true) {
                var path = new Array();

                for (var i = 0; i < this.length; i++) {
                    path.push(this.getAt(i));
                }
                self.all_OverLays.unshift(path);
                var poly = new google.maps.Polygon();
                poly.setPath(this);
                subgeopointService.Polygon(poly);
            } else {
                Polygon.setMap(null);
                var auxPolygon = new google.maps.Polygon();
                auxPolygon.setOptions(self.polyOptions);
                auxPolygon.setPath(self.all_OverLays[0]);

                Polygon = auxPolygon;

                Polygon.setMap(map);
                self.addOnDragEventPolygon(map, Polygon, self, subsidiaryList, subGeoPointList, subsidiaryID, subgeopointService);
            }
        });

        google.maps.event.addListener(Polygon.getPath(), 'set_at', function() {
            if (self.isOnDrag == false) {
                var pol_aux = new google.maps.Polygon();
                var path_ = new Array();

                for (var i = 0; i < this.length; i++) {
                    path_.push(this.getAt(i));
                }
                pol_aux.setPath(path_);

                if (self.ValidatePolygonPosition(subsidiaryList, subGeoPointList, pol_aux, subsidiaryID, self) == true) {
                    var path = new Array();

                    for (var i = 0; i < this.length; i++) {
                        path.push(this.getAt(i));
                    }
                    self.all_OverLays.unshift(path);
                    var poly = new google.maps.Polygon();
                    poly.setPath(this);
                    subgeopointService.Polygon(poly);
                } else {
                    Polygon.setMap(null);
                    var auxPolygon = new google.maps.Polygon();
                    auxPolygon.setOptions(self.polyOptions);
                    auxPolygon.setPath(self.all_OverLays[0]);

                    Polygon = auxPolygon;

                    Polygon.setMap(map);
                    self.addOnDragEventPolygon(map, Polygon, self, subsidiaryList, subGeoPointList, subsidiaryID, subgeopointService);
                }
            }
        });
    }

}