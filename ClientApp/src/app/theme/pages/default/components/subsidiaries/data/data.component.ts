import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { ActivatedRoute, Router } from '@angular/router';
import { } from 'googlemaps';
import { Notification } from '../../../../../../utils/notification';
import { DataComponentHelper } from './data.component.helper';
import { SubsidiaryService } from '../../../../../../_services/subsidiary.service';
import { finalize } from 'rxjs/operators';
import ValidationEngine from 'devextreme/ui/validation_engine';
import { RegionService } from '../../../../../../_services/region.service';
import { SubsidiaryGeoPointService } from '../../../../../../_services/subsidiarygeopoint.service';
import { ChangeDetectorRef } from '@angular/core';
import { LocalStorageUtil } from '../../../../../../utils/localstorageutil';

@Component({
    selector: 'app-data',
    templateUrl: './data.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit, AfterViewInit {
    @ViewChild('gmap') mapElement: ElementRef;
    map: google.maps.Map;

    private DataHelper: DataComponentHelper;
    private subsidiaryService: SubsidiaryService;
    private regionService: RegionService;
    private subsidiaryGeoPointService: SubsidiaryGeoPointService;

    namePattern: any = /^[a-zA-Z0-9]{2,}$/;
    WhiteSpacePattern: any = /^[^\s].+[^\s]$/;
    Radio: string = '100.00';
    LocateSubsidiary: any;
    private subsidiaryDataList: any;
    private subsidiaryID: number;
    private Position: any;
    private Polygon: any;
    public regiones: any[];
    public SubGeoPointList: any;
    public fieldReadOnly: false;

    public model = {
        id: 0,
        name: '',
        address: '',
        phoneNumber: '',
        idCompany: 0,
        latitude: '',
        longitude: '',
        hasGeofence: true,
        geofenceRadius: parseInt(this.Radio),
        idRegion: undefined,
        active: true,
        points: new Array()
    };

    constructor(subsidiaryService: SubsidiaryService, regionService: RegionService, subsidiaryGeoPointService: SubsidiaryGeoPointService, private route: ActivatedRoute, private router: Router, private cdRef: ChangeDetectorRef) {
        this.subsidiaryService = subsidiaryService;
        this.regionService = regionService;
        this.subsidiaryGeoPointService = subsidiaryGeoPointService;

        this.subsidiaryService.MarkerPosition$.subscribe((data) => {
            this.Position = data;
        });
        this.subsidiaryGeoPointService.Polygon$.subscribe((data) => {
            this.Polygon = data;
        });

        this.LocateSubsidiary = {
            icon: '/assets/app/media/img/subsidiary/locate.png',
            stylingMode: "text",
            onClick: () => {
                this.DataHelper.LookAddressAtMap(this.map, this.model.address, this.model.hasGeofence, this.Radio, this.model.id, this.subsidiaryDataList, this.subsidiaryService, this.SubGeoPointList);
            }
        };
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.subsidiaryID = parseInt(params['subsidiaryid']);
        });
    }

    async ngAfterViewInit() {
        await this.getRegions();
        await this.getSubGeoPoints();
        this.getSubsidiaries();
        this.cdRef.detectChanges();
    }

    checkBox_valueChanged(e) {
        this.DataHelper.setCircleOnClick(this.map, e.value, this.Radio, this.subsidiaryDataList, this.subsidiaryService, this.model.id, this.SubGeoPointList);
        var result = this.DataHelper.drawCircle(this.map, e.value, this.Radio, this.subsidiaryDataList, this.subsidiaryService, this.model.id, this.SubGeoPointList);
        if (result == false || result == undefined) {
            e.component.option("value", false);
            this.model.hasGeofence = false;
        } else {
            this.model.hasGeofence = true;
        }
    }

    LookAddres(e) {
        console.log(e.component.option("value"));
        this.DataHelper.LookAddressAtMap(this.map, e.component.option("value"), this.model.hasGeofence, this.Radio, this.model.id, this.subsidiaryDataList, this.subsidiaryService, this.SubGeoPointList);
    }

    getSubsidiaries() {
        Helpers.setLoading(true, 'Cargando ubicación de sucursales');
        this.subsidiaryService.getSubsidiaries().subscribe(
            result => {
                this.subsidiaryDataList = result.data;
                if (result.data.length > 0) this.subsidiaryDataList.sort(function(a, b) { return b.name - a.name; })
                if (this.subsidiaryID != 0) {
                    result.data.forEach(sub => {
                        if (this.subsidiaryID == sub.id) {
                            this.model.id = sub.id;
                            this.model.name = sub.name;
                            this.model.address = sub.address;
                            this.model.phoneNumber = sub.phoneNumber;
                            this.model.idCompany = sub.idCompany;
                            this.model.latitude = sub.latitude;
                            this.model.longitude = sub.longitude;
                            this.model.hasGeofence = sub.hasGeofence;
                            this.model.geofenceRadius = sub.geofenceRadius;
                            this.model.active = sub.active;
                            this.model.idRegion = sub.idRegion;
                            return;
                        }
                    });
                }

                this.initMap();
                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });
    }

    initMap() {
        let userLoginData = LocalStorageUtil.getUserLoginData();

        const mapProperties = {
            center: (this.subsidiaryID !== 0) ? new google.maps.LatLng(parseFloat(this.model.latitude), parseFloat(this.model.longitude))
                : new google.maps.LatLng(userLoginData.latitude, userLoginData.longitude),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
        this.DataHelper = new DataComponentHelper(this.map, this.subsidiaryService, this.subsidiaryGeoPointService);
        this.DataHelper.drawSubsidiaries(this.subsidiaryDataList, this.Radio, this.model, this.subsidiaryService, this.model.id, this.SubGeoPointList);
        if (this.model.id == 0) {
            this.DataHelper.setCircleOnClick(this.map, true, this.Radio, this.subsidiaryDataList, this.subsidiaryService, this.model.id, this.SubGeoPointList);
        }

        this.DataHelper.setDrawingMode(this.map, ((this.subsidiaryID == 0) ? true : false), this.SubGeoPointList, this.subsidiaryGeoPointService, this.subsidiaryID, this.subsidiaryDataList);
    }

    cleanModel() {
        this.model.id = 0;
        this.model.name = '';
        this.model.address = '';
        this.model.phoneNumber = '';
        this.model.idCompany = 0;
        this.model.latitude = '';
        this.model.hasGeofence = true;
        this.model.geofenceRadius = parseInt(this.Radio);
        this.model.idRegion = undefined;
        this.model.active = true;
    }

    NameComparison = () => {
        var auxlist = <any[]>this.subsidiaryDataList;
        var isValidName = true;

        auxlist.forEach(sub => {
            if (sub.name.toUpperCase() === this.model.name.toUpperCase() && sub.id != this.subsidiaryID) isValidName = false;
        });

        if (isValidName)
            return this.model.name;
        else
            return "";
    }

    returnToSubsidiary() {
        this.router.navigateByUrl('/subsidiaries/subsidiary');
    }

    mask_onValueChanged(e) {
        console.log(e);

    }

    SaveSubsidiary() {

        this.model.phoneNumber = this.model.phoneNumber.toString();
        let ValidationResult = ValidationEngine.validateGroup('submitVal');
        let ValidatePosition = (this.Position != undefined) ? true : false;

        if (ValidatePosition == false) {
            Notification.Error('Debe establecer posición geográfica de la sucursal');
        }

        if (ValidationResult.isValid && ValidatePosition) {
            if (this.model.id == 0) {
                Helpers.setLoading(true, 'Guardando Registro...');
                this.model.latitude = this.Position.lat();
                this.model.longitude = this.Position.lng();
                this.model.points = this.getSubsidiaryPoligon(0);

                this.subsidiaryService.insert(this.model).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res => {
                    this.returnToSubsidiary();
                }, error => {
                    Notification.Error('No se pudo realizar guardado de sucursal');
                });
            } else {
                Helpers.setLoading(true, 'Actualizando Registro...');
                this.model.latitude = this.Position.lat();
                this.model.longitude = this.Position.lng();

                this.model.points = this.getSubsidiaryPoligon(this.model.id);

                this.subsidiaryService.update(this.model).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res => {
                    this.returnToSubsidiary();
                }, error => {
                    Notification.Error('No se pudo actualizar información de sucursal');
                });
            }
        }
    }

    async getRegions() {
        let result = await this.regionService.getRegions().toPromise().catch(error => {
            Notification.WebMethodError(error);
            Helpers.setLoading(false);
        });

        if (result.data != undefined && result.data.length > 0) {
            this.regiones = result.data;
            this.regiones.sort(function(a, b) { return b.name - a.name; });
        } else {
            this.regiones = new Array();
        }
    }

    async getSubGeoPoints() {
        let result = await this.subsidiaryGeoPointService.getSubsidiariesGeoPoints().toPromise();

        if (result.data != undefined && result.data.length > 0) {
            this.SubGeoPointList = result.data;
        } else { this.SubGeoPointList = new Array(); }
    }

    getSubsidiaryPoligon(subid: number) {
        if (this.Polygon != undefined) {
            var subGeoPointModel: { id: number, idSubsidiary: number, latitude: any, longitude: any, active: boolean }[] = new Array();

            var auxSubList = this.DataHelper.groupBy(<any[]>this.SubGeoPointList, 'idSubsidiary');
            var count = 0;

            if (auxSubList.length > 0) {
                auxSubList = auxSubList.filter(sub => sub.key == subid);
                if (auxSubList.length > 0)
                    count = auxSubList[0].values.length;
            }

            for (var i = 0; i < this.Polygon.getPath().getLength(); i++) {
                var id_: number = 0;

                if (i < count) {
                    id_ = auxSubList[0].values[i].id;
                }
                subGeoPointModel.push({ id: id_, idSubsidiary: (count == 0) ? 0 : subid, latitude: this.Polygon.getPath().getAt(i).lat(), longitude: this.Polygon.getPath().getAt(i).lng(), active: true });
            }

            return subGeoPointModel;
        }

        return new Array();
    }
}