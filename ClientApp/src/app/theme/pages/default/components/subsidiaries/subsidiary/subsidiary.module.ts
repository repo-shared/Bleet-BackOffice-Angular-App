import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { Routes, RouterModule } from '@angular/router';
import { SubsidiaryComponent } from "./subsidiary.component";
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { SubsidiaryService } from '../../../../../../_services/subsidiary.service';
import { RegionService } from '../../../../../../_services/region.service';
import { SubsidiaryGeoPointService } from '../../../../../../_services/subsidiarygeopoint.service';
import { DxDataGridModule, DxSwitchModule, DxPopupModule, DxCheckBoxModule, DxTextBoxModule, DxLookupModule, DxDateBoxModule, DxValidatorModule, DxLoadPanelModule } from 'devextreme-angular';


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": SubsidiaryComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,
        DxSwitchModule,
        DxPopupModule,
        DxLookupModule,
        DxDateBoxModule,
        DxTextBoxModule,
        DxLoadPanelModule,
        DxValidatorModule,
        DxCheckBoxModule,
        DxDataGridModule,
        FormsModule
    ], exports: [
        RouterModule
    ], declarations: [
        SubsidiaryComponent
    ],
    providers: [SubsidiaryService, RegionService, SubsidiaryGeoPointService],
})
export class SubsidiaryModule {
}