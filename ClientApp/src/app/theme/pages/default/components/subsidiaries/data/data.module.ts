import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { DataComponent } from './data.component';
import { DxDataGridModule, DxSwitchModule, DxPopupModule, DxCheckBoxModule, DxTextBoxModule, DxLookupModule, DxDateBoxModule, DxValidatorModule, DxLoadPanelModule, DxSelectBoxModule, DxNumberBoxModule } from 'devextreme-angular';
import { SubsidiaryService } from '../../../../../../_services/subsidiary.service';
import { RegionService } from '../../../../../../_services/region.service';
import { SubsidiaryGeoPointService } from '../../../../../../_services/subsidiarygeopoint.service';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': DataComponent,
            },
        ],
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,
        DxSwitchModule,
        DxPopupModule,
        DxLookupModule,
        DxDateBoxModule,
        DxTextBoxModule,
        DxNumberBoxModule,
        DxLoadPanelModule,
        DxValidatorModule,
        DxCheckBoxModule,
        DxDataGridModule,
        DxSelectBoxModule,
        FormsModule
    ], exports: [
        RouterModule,
    ], declarations: [
        DataComponent,
    ], providers: [SubsidiaryService, RegionService, SubsidiaryGeoPointService]
})

export class DataModule {
}