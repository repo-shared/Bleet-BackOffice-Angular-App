import { AngularFirestore } from '@angular/fire/firestore';

export class UserComponentHelper {

    private collection: string = 'security.users';
    private db: AngularFirestore;

    constructor(db: AngularFirestore) {
        this.db = db;
    }

    getAll() {
        return this.db.firestore.collection(this.collection).get().then((querySnapshot) => {
            return querySnapshot.docs.map(doc => {
                return {
                    id: doc.id,
                    name: doc.data().name,
                    surname: doc.data().surname,
                    user: doc.data().user,
                    pass: doc.data().pass,
                    deviceId: doc.data().deviceId
                }
            });
        });
    }

    save(model) {
        return this.db.collection(this.collection).add(model).then(res => {
            console.log(res);
            return res;

        });
    }

    update(model) {
        return this.db.collection(this.collection).doc(model.id).update(model).then(res => {
            console.log(res);
            return res;
        });
    }

}