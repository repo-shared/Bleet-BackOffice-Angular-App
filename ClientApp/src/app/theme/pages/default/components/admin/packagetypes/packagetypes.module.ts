
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { Routes, RouterModule } from '@angular/router';
import { PackageTypesComponent } from "./packagetypes.component";
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../../../../../../../environments/environment';
import {
    DxScrollViewModule,
    DxDataGridModule, DxSwitchModule, DxPopupModule, DxCheckBoxModule, DxTextBoxModule, DxLookupModule, DxDateBoxModule, DxValidatorModule, DxLoadPanelModule, DxSelectBoxComponent, DxSelectBoxModule, DxNumberBoxModule,
    DxButtonModule,
} from 'devextreme-angular';
import { PackageTypeService } from '../../../../../../_services/packagetype.services';


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": PackageTypesComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        DxSwitchModule,
        DxPopupModule,
        DxLookupModule,
        DxDateBoxModule,
        DxSelectBoxModule,
        DxTextBoxModule,
        DxLoadPanelModule,
        DxValidatorModule,
        DxCheckBoxModule,
        DxDataGridModule,
        FormsModule,
        DxNumberBoxModule,
        DxScrollViewModule,
        DxButtonModule
    ], exports: [
        RouterModule
    ], declarations: [
        PackageTypesComponent
    ],
    providers: [PackageTypeService, DatePipe],
})
export class PackageTypesModule {
}
