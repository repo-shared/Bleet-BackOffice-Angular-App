import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { SchedulesService } from '../../../../../../_services/schedule.services';
import { WorkDayHourService } from '../../../../../../_services/workdayhour.services';
import { WorkScheduleUserService } from '../../../../../../_services/workscheduleuser.services';
import { UserService } from '../../../../../../_services/user.services';
import { Helpers } from '../../../../../../helpers';
import { Notification } from '../../../../../../utils/notification';
import ValidationEngine from 'devextreme/ui/validation_engine';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-schedules',
    templateUrl: './schedules.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./schedules.component.css']
})
export class SchedulesComponent implements OnInit {
    public schedulesList = new Array();
    public usersList = new Array();
    public workDayHourList = new Array();
    public workingDayHourList = new Array();
    public scheduleUserList = new Array();

    public popupDataVisible: boolean = false;
    public popupDataVisible_Turno: boolean = false;
    public popupDataVisible_Usuario: boolean = false;
    public addTurno: boolean = false;

    public dayList = new Array();
    private scheduleService: SchedulesService;
    private userService: UserService;
    private workdayhourService: WorkDayHourService;
    private workscheduleuserService: WorkScheduleUserService;
    private scheduleID: number = 0;
    WhiteSpacePattern: any = /^[^\s].+[^\s]$/;
    private time: number = 5000;

    public model = {
        id: 0,
        name: '',
        active: true
    };

    public Turno_Model = {
        id: 0,
        idWorkSchedule: 0,
        dayWeek: undefined,
        hourStart: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDay(), 6),
        hourEnd: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDay(), 7),
        active: true
    };

    constructor(_scheduleService: SchedulesService, _userService: UserService, _workdayhour: WorkDayHourService, _workscheduleuser: WorkScheduleUserService, private cdRef: ChangeDetectorRef) {
        this.scheduleService = _scheduleService;
        this.userService = _userService;
        this.workdayhourService = _workdayhour;
        this.workscheduleuserService = _workscheduleuser;

        this.dayList.push({ id: 1, day: 'Lunes' });
        this.dayList.push({ id: 2, day: 'Martes' });
        this.dayList.push({ id: 3, day: 'Mi\u00e9rcoles' });
        this.dayList.push({ id: 4, day: 'Jueves' });
        this.dayList.push({ id: 5, day: 'Viernes' });
        this.dayList.push({ id: 6, day: 'S\u00e1bado' });
        this.dayList.push({ id: 7, day: 'Domingo' });
    }

    ngOnInit() {
        this.getSchedules();
        this.getUsers();
        this.getDayHourSchedule();
        this.getUserSchedule();
    }

    getSchedules(showMessage?) {
        if (showMessage == undefined)
            Helpers.setLoading(true, 'Cargando horarios');
        this.scheduleService.getSchedules().subscribe(
            result => {
                this.schedulesList = result.data;
                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });
    }

    getUsers() {
        this.userService.getAll().subscribe(
            result => {
                this.usersList = result.data;
                console.log(this.usersList);
                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });
    }

    getDayHourSchedule() {
        this.workdayhourService.getWorkDayHour().subscribe(
            result => {
                this.workDayHourList = result.data;
                if (this.workDayHourList.length > 0) {
                    this.workDayHourList.forEach(item => {
                        this.dayList.forEach(day => {
                            if (item.dayWeek == day.id) {
                                item["DayName"] = day.day;
                                return;
                            }
                        });

                        item["timeStart"] = this.toTimestamp(item.hourStart);
                        item["timeEnd"] = this.toTimestamp(item.hourEnd);
                    });
                }
                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });
    }

    getUserSchedule() {
        this.workscheduleuserService.getUserSchedules().subscribe(
            result => {
                this.scheduleUserList = result.data;
                console.log(this.scheduleUserList);
                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });
    }

    NameComparison = () => {
        var auxlist = <any[]>this.schedulesList;
        var isValidName = true;

        auxlist.forEach(item => {
            if (item.name.toUpperCase() === this.model.name.toUpperCase() && item.id != this.scheduleID) isValidName = false;
        });

        if (isValidName)
            return this.model.name;
        else
            return "";
    }

    SaveSchedule() {
        let ValidationResult = ValidationEngine.validateGroup('submitVal');

        if (ValidationResult.isValid == false)
            return;

        //if (this.workingDayHourList.length == 0) {
        //    Notification.Error('Debe establecer al menos un turno en el horario');
        //    return;
        //}

        if (this.model.id == 0) {
            Helpers.setLoading(true, 'Guardando Registro...');

            this.scheduleService.insert(this.model).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res => {
                this.getSchedules('load');
                this.popupDataVisible = false;
                this.getDayHourSchedule();
                this.clearSecheduleModel();
            }, error => {
                Notification.Error('No se pudo realizar guardado de horario');
            });
        } else {
            Helpers.setLoading(true, 'Actualizando Registro...');

            this.scheduleService.update(this.model).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res => {
                var InsertModel = new Array();
                var UpdateModel = new Array();

                this.workingDayHourList.forEach(item => {
                    if (item.id == 0) {
                        InsertModel.push(item);
                    } else {
                        UpdateModel.push(item);
                    }
                });

                if (InsertModel.length > 0) {
                    this.workdayhourService.insert(InsertModel).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res_ => {
                    }, error => {
                        Notification.Error('No se pudo realizar guardado de turnos');
                    });
                }

                if (UpdateModel.length > 0) {
                    this.workdayhourService.update(UpdateModel).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res_ => {
                    }, error => {
                        Notification.Error('No se pudo actualizar datos de turnos');
                    });
                }
                this.getSchedules('load');
                this.getDayHourSchedule();
                this.clearSecheduleModel();
                this.popupDataVisible = false;
            }, error => {
                Notification.Error('No se pudo actualizar datos de horario');
            });
        }
    }

    SaveTurno() {
        let ValidationResult_ = ValidationEngine.validateGroup('submitVal_Turno');

        if (ValidationResult_.isValid == false) {
            return;
        }

        if (this.ValidateDayHour(this.workingDayHourList, this.Turno_Model.dayWeek, this.Turno_Model.hourStart, this.Turno_Model.hourEnd, true) == false) {
            return;
        }
        if (this.Turno_Model.id == 0) {
            var auxTurno = {
                id: this.Turno_Model.id,
                idWorkSchedule: this.scheduleID,
                dayWeek: this.Turno_Model.dayWeek,
                hourStart: this.Turno_Model.hourStart,
                hourEnd: this.Turno_Model.hourEnd,
                active: true
            }

            this.workingDayHourList.push(auxTurno);

            this.workingDayHourList.forEach(item => {
                if (item == auxTurno) {
                    this.dayList.forEach(day => {
                        if (item.dayWeek == day.id) {
                            item["DayName"] = day.day;
                            return;
                        }
                    });

                    item["timeStart"] = this.toTimestamp(item.hourStart);
                    item["timeEnd"] = this.toTimestamp(item.hourEnd);
                }
            });
        } else {
            this.workingDayHourList.forEach(item => {
                if (item.id == this.Turno_Model.id) {
                    this.dayList.forEach(day => {
                        if (item.dayWeek == day.id) {
                            item["DayName"] = day.day;
                            return;
                        }
                    });
                    item.hourStart = this.Turno_Model.hourStart;
                    item.hourEnd = this.Turno_Model.hourEnd;
                    item["timeStart"] = this.toTimestamp(item.hourStart);
                    item["timeEnd"] = this.toTimestamp(item.hourEnd);
                }
            });
        }

        this.popupDataVisible_Turno = false;
        this.cdRef.detectChanges();
        this.clearTurnoModel();
        this.addTurno = true;
    }

    ValidateDayHour(workingList: any, day: number, hourStart: any, hourEnd: any, validateDays?: any) {
        if (workingList == undefined)
            workingList = new Array();

        var isInRestrictedSchedule: { name: string, isRestriced: boolean, validateProces: number }[] = new Array();

        workingList.forEach(dh => {
            if (dh.dayWeek == day && new Date(dh.hourStart).getTime() == new Date(hourStart).getTime() && new Date(dh.hourEnd).getTime() == new Date(hourEnd).getTime()) {
                isInRestrictedSchedule.push({ name: '', isRestriced: true, validateProces: 1 })
            }

            var HoraInicio = new Date(dh.hourStart).getHours();
            var HoraFinal = new Date(dh.hourEnd).getHours();
            var valHoraInicio = new Date(hourStart).getHours();
            var valHoraFinal = new Date(hourEnd).getHours();

            var MinInicio = new Date(dh.hourStart).getMinutes();
            var MinFinal = new Date(dh.hourEnd).getMinutes();
            var valMinInicio = new Date(hourStart).getMinutes();
            var valMinFinal = new Date(hourEnd).getMinutes();

            var FechaInicio = new Date(2019, 11, (9 + dh.dayWeek), HoraInicio, MinInicio);
            var FechaFinal = new Date(2019, 11, (9 + dh.dayWeek), HoraFinal, MinFinal);

            var valFechaInicio = new Date(2019, 11, (9 + day), valHoraInicio, valMinInicio);
            var valFechaFinal = new Date(2019, 11, (9 + day), valHoraFinal, valMinFinal);

            if (FechaInicio.getTime() <= valFechaInicio.getTime() && valFechaInicio.getTime() <= FechaInicio.getTime()) {
                isInRestrictedSchedule.push({ name: '', isRestriced: true, validateProces: 2 });
            }
            else if (FechaInicio.getTime() <= valFechaFinal.getTime() && valFechaFinal.getTime() <= FechaInicio.getTime()) {
                isInRestrictedSchedule.push({ name: '', isRestriced: true, validateProces: 2 });
            }
            else if (FechaFinal.getTime() <= valFechaInicio.getTime() && valFechaInicio.getTime() <= FechaFinal.getTime()) {
                isInRestrictedSchedule.push({ name: '', isRestriced: true, validateProces: 2 });
            }
            else if (FechaFinal.getTime() <= valFechaFinal.getTime() && valFechaFinal.getTime() <= FechaFinal.getTime()) {
                isInRestrictedSchedule.push({ name: '', isRestriced: true, validateProces: 2 });
            }
        });

        if (validateDays != undefined) {
            if (hourStart == hourEnd) {
                isInRestrictedSchedule.push({ name: '', isRestriced: true, validateProces: 1 })
                Notification.CustomError('Hora inicio no puede ser igual a hora fin', this.time);
                return false;
            }
            else if (hourEnd <= hourStart) {
                isInRestrictedSchedule.push({ name: '', isRestriced: true, validateProces: 1 })
                Notification.CustomError('Hora final no puede ser antes que hora inicio', this.time);
                return false;
            }

            if (isInRestrictedSchedule.findIndex(zone => zone.isRestriced == true) > -1) {
                Notification.CustomError('No se puede establecer el turno indicado, debido a que coincide con otro turno', this.time);
                return false;
            }
            else {
                return true;
            }
        } else {
            if (isInRestrictedSchedule.findIndex(zone => zone.isRestriced == true) > -1) {
                return false;
            }
            else {
                return true;
            }
        }
    }

    clearTurnoModel() {
        ValidationEngine.resetGroup('submitVal_Turno');
        this.Turno_Model.id = 0;
        this.Turno_Model.idWorkSchedule = 0;
        this.Turno_Model.dayWeek = undefined;
        this.Turno_Model.hourStart = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDay(), 6);
        this.Turno_Model.hourEnd = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDay(), 7);
        this.Turno_Model.active = true;
    }

    clearSecheduleModel() {
        ValidationEngine.resetGroup('submitVal');
        this.model.id = 0;
        this.model.name = '';
        this.model.active = true;
        this.workingDayHourList = new Array();
        this.scheduleID = 0;
    }

    groupBy(xs, key) {
        return xs.reduce(function(rv, x) {
            let v = key instanceof Function ? key(x) : x[key]; let el = rv.find((r) => r && r.key === v);
            if (el) {
                el.values.push(x);
            }
            else {
                rv.push({ key: v, values: [x] });
            }
            return rv;
        }, []);
    }

    toTimestamp(strDate) {
        var hora = new Date(strDate).getHours();
        var minutos = new Date(strDate).getMinutes();
        var time = (hora > 12) ? "PM" : "AM";
        hora = (hora >= 13) ? (hora - 12) : hora;
        return ("0" + hora).slice(-2) + ":" + ("0" + minutos).slice(-2) + " " + time;
    }

    edit_schedule(e) {
        this.scheduleID = e.id;
        this.model.id = e.id;
        this.model.name = e.name;
        this.model.active = true;
        var auxlist = this.groupBy(<any[]>this.workDayHourList, 'idWorkSchedule');

        auxlist.forEach(item => {
            if (item.key == this.model.id) {
                this.workingDayHourList = item.values;
            }
        });

        this.popupDataVisible = true;
    }

    edit_turn(e) {
        this.Turno_Model.active = e.active;
        this.Turno_Model.dayWeek = e.dayWeek;
        this.Turno_Model.hourEnd = e.hourEnd;
        this.Turno_Model.hourStart = e.hourStart;
        this.Turno_Model.idWorkSchedule = e.idWorkSchedule;
        this.Turno_Model.id = e.id;

        this.popupDataVisible_Turno = true;
    }

    edit_user(e) {
        this.scheduleID = e.id;

        var auxlist = this.groupBy(<any[]>this.scheduleUserList, 'idWorkSchedule');

        auxlist.forEach(item => {
            if (item.key == this.scheduleID) {
                this.usersList.forEach(subitem => {
                    item.values.forEach(itema => {
                        if (subitem.id == itema.idUser) {
                            subitem["scheduleactive"] = itema.active;
                            subitem["idSchedule"] = this.scheduleID;
                            subitem["id_"] = itema.id;
                        }
                    });
                });
            }
        });

        this.usersList.forEach(subitem => {
            if (subitem.scheduleactive == undefined) {
                subitem["scheduleactive"] = false;
            }
            else if (subitem.scheduleactive != undefined && subitem.idSchedule != this.scheduleID) {
                subitem["scheduleactive"] = false;
            }
        });

        this.popupDataVisible_Usuario = true;
    }

    Save_User() {
        var auxlist = this.groupBy(<any[]>this.usersList, 'idSchedule');

        var InsertModel = new Array();
        var UpdateModel = new Array();

        auxlist.forEach(item => {
            if (item.key == this.scheduleID) {
                item.values.forEach(sub => {
                    var model_Usuario = {
                        id: 0,
                        active: true,
                        idUser: '',
                        idWorkSchedule: 0
                    };
                    if (sub.id_ == undefined) {
                        model_Usuario.id = 0;
                        model_Usuario.active = sub.scheduleactive;
                        model_Usuario.idUser = sub.id;
                        model_Usuario.idWorkSchedule = this.scheduleID;

                        InsertModel.push(model_Usuario);
                    } else {

                        model_Usuario.id = sub.id_;
                        model_Usuario.active = sub.scheduleactive;
                        model_Usuario.idUser = sub.id;
                        model_Usuario.idWorkSchedule = this.scheduleID;

                        UpdateModel.push(model_Usuario);
                    }
                });
            }
            else if (item.key == undefined) {
                item.values.forEach(sub => {

                    if (sub.scheduleactive == true) {
                        var model_Usuario = {
                            id: 0,
                            active: true,
                            idUser: '',
                            idWorkSchedule: 0
                        };

                        model_Usuario.id = 0;
                        model_Usuario.active = sub.scheduleactive;
                        model_Usuario.idUser = sub.id;
                        model_Usuario.idWorkSchedule = this.scheduleID;

                        InsertModel.push(model_Usuario);
                    }
                });
            }
        });

        Helpers.setLoading(true, 'Actualizando Registro...');
        if (InsertModel.length > 0) {
            this.workscheduleuserService.insert(InsertModel).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res_ => {
                this.getUserSchedule();
                this.popupDataVisible_Usuario = false;
            }, error => {
                Notification.Error('No se pudo realizar asignaci\u00F3n de usuarios');
            });
        }

        if (UpdateModel.length > 0) {
            this.workscheduleuserService.update(UpdateModel).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res_ => {
                this.getUserSchedule();
                this.popupDataVisible_Usuario = false;
            }, error => {
                Notification.Error('No se pudo actualizar asignaci\u00F3n de usuarios');
            });
        }


    }

    checkBox_valueChanged(event, e) {
        if (event.value == true) {
            let result = this.ValidateUserSchedule(e.id, this.scheduleID);
            if (result == false || result == undefined) {
                e.scheduleactive = false;
                this.cdRef.detectChanges();
            }
        }
    }

    ValidateUserSchedule(idUser: any, idSchedule: any) {
        var auxlist = this.groupBy(<any[]>this.scheduleUserList, 'idUser');

        if (auxlist == undefined)
            return true;

        var isInRestrictedSchedule: { name: string, isRestriced: boolean, validateProces: number }[] = new Array();
        auxlist.forEach(item => {
            if (item.key == idUser) {
                var auxlist_ = this.groupBy(<any[]>this.workDayHourList, 'idWorkSchedule');
                var schedule;

                auxlist_.forEach(subitem => {
                    if (subitem.key == idSchedule)
                        schedule = subitem.values;
                });

                if (schedule == undefined)
                    return true;

                var auxArray = new Array();

                item.values.forEach(sub => {
                    if (sub.active == true && sub.idWorkSchedule != idSchedule) {
                        auxlist_.forEach(tem => {
                            if (sub.idWorkSchedule == tem.key) {
                                auxArray.push(tem);
                            }
                        });
                    }
                });

                auxArray.forEach(sub => {
                    if (this.ValidateDayHour(schedule, sub.dayWeek, sub.hourStart, sub.hourEnd) == false) {
                        isInRestrictedSchedule.push({ name: '', isRestriced: true, validateProces: 2 });
                    }
                });
            }
        });

        if (isInRestrictedSchedule.findIndex(zone => zone.isRestriced == true) > -1) {
            Notification.CustomError('No se puede asignar el usuario a este turno, debido a que coincide con otro turno', this.time);
            return false;
        }
        else {
            return true;
        }
    }

    clearUsers() {
        this.usersList.forEach(sub => {
            delete sub["scheduleactive"];
            delete sub["idSchedule"];
        });
    }

    checkBox_valueChanged_Horario(event) {
        if (event.value == false) {
            var auxlist = this.groupBy(<any[]>this.scheduleUserList, 'idWorkSchedule');

            auxlist.forEach(sub => {
                if (sub.key == this.scheduleID) {
                    sub.values.forEach(item => {
                        if (item.active == true) {
                            event.component.option("value", true);
                            this.model.active = true;
                            Notification.CustomError('El horario no puede ser desactivado, debido a que posee usuarios activos', this.time);
                            this.cdRef.detectChanges();
                        }
                    });
                }
            });
        }

    }
}