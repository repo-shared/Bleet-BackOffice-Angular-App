import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { } from 'googlemaps';
import { UserComponentHelper } from './users.component.helper';
import { UserService } from '../../../../../../_services/user.services';
import { DxDataGridModule } from 'devextreme-angular';
import { Notification } from '../../../../../../utils/notification';
import { DxDataGridComponent } from 'devextreme-angular';
import { finalize } from 'rxjs/operators';
import { ChangeDetectorRef } from '@angular/core';
import { DateUtil } from '../../../../../../utils/dateutil';



@Component({
    selector: "app-admin-users",
    templateUrl: "./users.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, AfterViewInit {

    private userHelper: UserComponentHelper
    public popupDataVisible: boolean = false;
    public popupTurnosVisible: boolean = false;
    @ViewChild(DxDataGridComponent, { static: true }) gridContainer: DxDataGridComponent;

    public model = {
        id: '',
        idCompany: 0,
        idSubsidiary: 0,
        userName: '',
        passw: '',
        fullName: '',
        firstName: '',
        lastName: '',
        genre: '',
        email: '',
        birthDate: '1994-05-05',
        phoneNumber: ''
    };
    public modelWorkSchedule = {
        id: 0,
        idUser: '',
        idWorkSchedule: 0,
        active: true
    }

    public modelIdWorkUser = {
        parent: '',
        parentId: 0,
        data: []
    }

    public dataWor = new Array();
    public workDayHourData = new Array();
    public WorkDayHourList = new Array();
    public WorkScheduleList = new Array();
    public workScheduleUserList = new Array();
    public modelWorkScheduleList = new Array();
    public genderList;
    public userList;
    private _phoneNumberList: any[] = [];
    private emailList: any[] = [];
    private workScheduleData: any[] = [];
    public idWorkScheduleUser = new Array();

    _save: boolean = false;


    constructor(private _script: ScriptLoaderService, db: AngularFirestore, private userSrv: UserService, private cdRef: ChangeDetectorRef) {
        this.userHelper = new UserComponentHelper(db);

    }

    ngOnInit() {
        this.get_users();
        this.genderList = this.userSrv.getGenders();
    }

    ngAfterViewInit() {

    }

    get_users() {
        Helpers.setLoading(true, 'Cargando Usuarios...');
        this.userSrv.getAll().subscribe(res => {
            this.userList = res.data;
            this.model.id = this.userList.id;
            this.model.idCompany = this.userList.idCompany;
            this.model.idSubsidiary = this.userList.idSubsidiary;
            Helpers.setLoading(false);
        });
    }

    save_user() {
        Helpers.setLoading(true, 'Guardando Usuario...');

        if (this.model.firstName === '') {
            Helpers.setLoading(false);
            return Notification.Error("Debe ingresar un nombre");
        }

        if (this.model.lastName === '') {
            Helpers.setLoading(false);
            return Notification.Error("Debe ingresar un apellido");
        }

        if (this.model.genre === '') {
            Helpers.setLoading(false);
            return Notification.Error("Debe seleccionar el genero");
        }

        if (this.model.birthDate === '') {
            Helpers.setLoading(false);
            return Notification.Error("Debe ingresar la fecha de nacimiento");
        }

        if (this.model.phoneNumber === '') {
            Helpers.setLoading(false);
            return Notification.Error("Debe ingresar el tel\u00E9fono");
        }

        if (this.model.userName === '') {
            Helpers.setLoading(false);
            return Notification.Error("Debe ingresar el nombre de usuario");
        }

        if (this.model.passw === "") {
            Helpers.setLoading(false);
            return Notification.Error("Debe ingresar la contrase\u00F1a");
        }

        if (this.model.userName.length < 6) {
            Helpers.setLoading(false);
            return Notification.Error("Debe ingresar al menos 6 caracteres en el nombre de usuario");
        }

        if (this.model.passw.length < 6) {
            Helpers.setLoading(false);
            return Notification.Error("Debe ingresar al menos 6 caracteres en la contrase\u00F1a");
        }

        this.findPhoneNumber();
        this.findEmail();

        if (this._phoneNumberList.length > 0) {
            this._phoneNumberList = [];
            Helpers.setLoading(false);
            return Notification.Error("El tel\u00E9fono ya existe, ingresar uno nuevo");
        }

        if (this.emailList.length > 0) {
            this.emailList = [];
            Helpers.setLoading(false);
            return Notification.Error("El correo ya existe, ingresar uno nuevo");
        }

        this.userSrv.update(this.model).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res => {
            Helpers.setLoading(false);
            this.popupDataVisible = false;
            this.get_users();
        },
            error => {
                Notification.WebMethodError(error);
                console.log(error);
                Helpers.setLoading(false);
            }
        );
    }

    edit_user_init(e) {
        this.model = e;
        this.model.passw = '';
        this.popupDataVisible = true;
    }

    get_WorkDayHours(e) {
        var info = e.selectedRowsData[0];
        return new Promise((resolve) => {
            this.userSrv.getWorkDayHoursId(info.id).subscribe(res => {
                res.data.forEach(el => {
                    el.dayName = DateUtil.getWeekDayById(el.dayWeek).day;
                });

                this.WorkDayHourList = res.data;

                Helpers.setLoading(false);
                resolve();
            },
                error => {
                    Notification.WebMethodError(error);
                    Helpers.setLoading(false);
                });

        });
    }

    get_WorkSchedules() {

        return new Promise((resolve) => {
            this.userSrv.getWorkSchedules().subscribe(res => {
                this.workScheduleData = res.data;
                this.workScheduleData.forEach(sh => {
                    if (this.workScheduleUserList.filter(x => x.id == sh.id).length == 0) {
                        sh.asignado = false;
                        this.workScheduleUserList.push(sh);
                    }
                });
                Helpers.setLoading(false);
                resolve();
            },
                error => {
                    Notification.WebMethodError(error);
                    Helpers.setLoading(false);
                });
        });
    }

    get_union(id: string) {
        this.popupTurnosVisible = true;
        this.get_WorkSchedulesByIdUser(id).then(() => {
            this.get_WorkSchedules();
        });
    }

    get_WorkSchedulesByIdUser(id: string) {
        return new Promise((resolve) => {
            this.userSrv.getByIdUserWorkSchedule(id).subscribe(res => {

                if (res.data.length === 0) {
                    return;
                }
                res.data[0].workSchedule.forEach(sh => {
                    sh.asignado = true;
                    this.workScheduleUserList.push(sh);
                });
                this.cdRef.detectChanges();
                Helpers.setLoading(false);
                resolve();
            },
                error => {
                    Notification.WebMethodError(error);
                    Helpers.setLoading(false);
                });
        });
    }

    save_WorkScheduleUser() {

        var newDataToInsert = new Array();
        var newDataToDelete = new Array();

        this.workScheduleUserList.forEach(sh => {
            newDataToDelete.push(sh.id);
        });


        this.workScheduleUserList.filter(f => f.asignado == true).forEach(sh => {
            newDataToInsert.push({
                id: 0,
                idUser: this.model.id,
                idWorkSchedule: sh.id,
                active: true
            })
        });

        this.modelIdWorkUser.data = newDataToDelete;
        this.delete_works(this.modelIdWorkUser, newDataToInsert);
    }

    delete_works(ids, newDataToInsert) {
        this.userSrv.deleteWorkScheduleUser(ids).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res => {
            Helpers.setLoading(false);
            this.popupTurnosVisible = false;
            this.popupDataVisible = false;
            this.get_users();
            this.cleanModel();
            this.save_workS(newDataToInsert);
        },
            error => {
                Notification.WebMethodError(error);
                console.log(error);
                Helpers.setLoading(false);
            }
        );

    }

    save_workS(data) {

        this.userSrv.postWorkScheduleUser(data).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res => {
            this._save = true;
        },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });
    }

    findPhoneNumber() {
        this.userList.forEach(usr => {
            if (this.model.phoneNumber === usr.phoneNumber && this.model.id !== usr.id) {
                this._phoneNumberList.push(usr);
            }
        });
    }

    findEmail() {
        this.userList.forEach(usr => {
            if (this.model.email === usr.email && this.model.id !== usr.id) {
                this.emailList.push(usr);
            }
        });
    }

    cleanModel() {
        this.workScheduleData = [];
        this.workScheduleUserList = [];
    }
}