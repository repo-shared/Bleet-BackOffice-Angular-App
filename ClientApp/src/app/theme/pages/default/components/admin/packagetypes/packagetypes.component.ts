import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { } from 'googlemaps';
import { PackageTypeService } from '../../../../../../_services/packagetype.services';
import { DxDataGridComponent } from 'devextreme-angular';
import { Notification } from '../../../../../../utils/notification';
import { finalize } from 'rxjs/operators';
import { PackageType } from '../../../../../../_interfaces/package-type.interfaces';


@Component({
    selector: "app-admin-packagetype",
    templateUrl: "./packagetypes.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./packagetypes.component.css']
})
export class PackageTypesComponent implements OnInit, AfterViewInit {


    public popupDataVisible: boolean = false;
    @ViewChild(DxDataGridComponent, { static: true }) gridContainer: DxDataGridComponent;


    public dataType: PackageType = {
        id: 0,
        idCompany: 0,
        typeName: "",
        active: true
    }


    public packagesList: PackageType[] = [];
    public packageTypeFilter: PackageType[] = [];

    applySearch: boolean = true;

    constructor(private _script: ScriptLoaderService,
        private pkgsSrv: PackageTypeService) {

    }

    ngOnInit() {
        this.get_packages();
    }

    ngAfterViewInit() {

    }

    get_packages() {
        Helpers.setLoading(true, 'Cargando Tipos de paquetes...');

        return new Promise((resolve) => {

            this.pkgsSrv.getAll().subscribe(res => {
                this.packagesList = res.data;
                this.packagesList.sort(function(a, b) { return b.id - a.id; })
                this.packagesList = res.data;
                Helpers.setLoading(false);
                resolve();
            },
                error => {
                    Notification.WebMethodError(error);
                    Helpers.setLoading(false);
                });
        });
    }

    save_packagetype() {

        Helpers.setLoading(true, 'Guardando Registro...');

        let packageType: string;

        packageType = this.dataType.typeName;
        packageType = packageType.trim();
        if (packageType == '') {
            Helpers.setLoading(false);
            return Notification.Error("Ingresar el tipo de paquete");

        }
        if (packageType.length < 2) {
            Helpers.setLoading(false);
            return Notification.Error("Debe ingresar al menos 2 caracteres");
        }

        if (this.dataType.id != 0) {

            this.apply_search(packageType, 'flag');
            if (this.packageTypeFilter.length > 0) {

                this.packageTypeFilter.forEach(prod => {

                    let term: string;
                    term = prod.typeName;
                    term = term.toLocaleLowerCase();

                    if (prod.id == this.dataType.id) {

                        this.dataType.typeName = packageType;
                        this.pkgsSrv.update(this.dataType).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res => {
                            Helpers.setLoading(false);
                            this.popupDataVisible = false;
                            this.get_packages();
                        },
                            error => {
                                Notification.WebMethodError(error);
                                Helpers.setLoading(false);
                            });

                    } else {
                        Helpers.setLoading(false);
                        this.applySearch = false;
                        return Notification.Error("No se puede registrar la categor\u00EDa porque ya existe");
                    }
                });

            } else {
                this.dataType.typeName = packageType;
                this.pkgsSrv.update(this.dataType).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res => {
                    Helpers.setLoading(false);
                    this.popupDataVisible = false;
                    this.get_packages();
                },
                    error => {
                        Notification.WebMethodError(error);
                        Helpers.setLoading(false);
                    });
            }
        } else {

            this.apply_search(this.dataType.typeName);
            if (!this.applySearch) return;

            this.dataType.id = 0;
            this.dataType.idCompany = 0;
            this.dataType.typeName;
            this.pkgsSrv.insert(this.dataType).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res => {
                this.popupDataVisible = false;
                this.get_packages();
            },
                error => {
                    Notification.WebMethodError(error);
                    Helpers.setLoading(false);
                });
        }
    }

    search_package(term: string) {

        if (this.packagesList.length === 0) {
            // Calling getPackage
            this.get_packages().then(() => {
                //Apply Filter
                this.filterPackageType(term);
            });
        } else {
            //Apply Filter
            this.filterPackageType(term);
        }
    }

    filterPackageType(term: string) {

        let typeName: string = '';
        this.packageTypeFilter = [];
        term = term.toLocaleLowerCase();

        this.packagesList.forEach(prod => {
            typeName = prod.typeName;
            typeName = typeName.toLocaleLowerCase();

            if (term === typeName) {
                this.packageTypeFilter.push(prod);
            }
        });

    }

    edit_packagetype_init(e) {

        this.dataType.id = e.id;
        this.dataType.idCompany = e.idCompany;
        this.dataType.typeName = e.typeName;
        this.dataType.active = e.active;
        this.popupDataVisible = true;
    }


    apply_search(term: string, flag?: string) {
        term = term.trim();
        this.search_package(term);

        if (flag) {

            return this.packageTypeFilter;

        } else {
            if (this.packageTypeFilter.length > 0) {
                this.packageTypeFilter = [];
                Helpers.setLoading(false);
                this.applySearch = false;
                return Notification.Error("No se puede registrar la categor\u00EDa porque ya existe");
            }
        }
    }

    cleanModel() {
        this.dataType.id = 0;
        this.dataType.idCompany = 0;
        this.dataType.typeName = '';
        this.dataType.active = true;
    }

}

