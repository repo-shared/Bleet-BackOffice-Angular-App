import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { } from 'googlemaps';
import { DxDataGridComponent } from 'devextreme-angular';
import { Notification } from '../../../../../../utils/notification';
import { RejectionTypeService } from '../../../../../../_services/rejectionType.services';
import { finalize } from 'rxjs/operators';
import { RejectionType } from '../../../../../../_interfaces/rejectionType.interfaces';

@Component({
    selector: "app-admin-rejectionType",
    templateUrl: "./rejectionType.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./rejectionType.component.css']
})

export class RejectionTypeComponent implements OnInit, AfterViewInit {

    @ViewChild(DxDataGridComponent, { static: true }) gridContainer: DxDataGridComponent;

    public popupDataVisible: boolean = false;
    public activeDisabled: boolean = false;

    public model: RejectionType = {
        id: 0,
        idCompany: 0,
        rejectionName: "",
        active: true
    }

    public rejectionTypeList;
    objeto: any;

    constructor(private _script: ScriptLoaderService,
        private rjTpSrv: RejectionTypeService) {

    }

    ngOnInit() {
        this.get_rejectionTypes();
    }

    ngAfterViewInit() {

    }

    get_rejectionTypes() {
        Helpers.setLoading(true, 'Cargando Motivos de Rechazo...');

        return new Promise((resolve) => {

            this.rjTpSrv.getAll().subscribe(res => {
                this.rejectionTypeList = res.data;
                this.rejectionTypeList.sort(function(a, b) { return b.id - a.id; })
                this.rejectionTypeList = res.data;
                Helpers.setLoading(false);
                resolve();
            },
                error => {
                    Notification.WebMethodError(error);
                    Helpers.setLoading(false);
                });
        });
    }

    save_rejectionType() {

        Helpers.setLoading(true, 'Guardando Motivo de rechazo...');

        this.model.rejectionName = this.model.rejectionName.trim();

        if (this.model.rejectionName == '') {
            Notification.Error('Motivo de rechazo debe de tener una descripci�n.');
            Helpers.setLoading(false);
        }
        else if (this.model.rejectionName.length < 2) {
            Notification.Error('Debe ingresar al menos 2 caracteres.');
            Helpers.setLoading(false);
        }
        else {
            this.model.idCompany = 0;

            if (this.model.id == 0) {

                //Verifica que no existan registros con la misma descripci�n.
                this.objeto = this.rejectionTypeList.filter(x => x.rejectionName == this.model.rejectionName && x.id != '');

                if (typeof this.objeto !== 'undefined' && this.objeto.length > 0) {
                    Notification.Error('Motivo de rechazo ya existe.');
                    Helpers.setLoading(false);
                }
                else {

                    this.rjTpSrv.insert(this.model).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res => {
                        Notification.Success('Motivo de rechazo Guardado.');
                        Helpers.setLoading(false);
                        this.popupDataVisible = false;
                        this.clear_model();
                        this.get_rejectionTypes();
                    },
                        error => {
                            Notification.WebMethodError(error);
                            Helpers.setLoading(false);
                        });
                }

            }
            else {

                //Verifica que no existan registros con la misma descripci�n.
                this.objeto = this.rejectionTypeList.filter(x => x.rejectionName == this.model.rejectionName && x.id != this.model.id);

                if (typeof this.objeto !== 'undefined' && this.objeto.length > 0) {
                    Notification.Error('Motivo de rechazo ya existe.');
                    Helpers.setLoading(false);
                }
                else {

                    this.rjTpSrv.update(this.model).pipe(finalize(() => { Helpers.setLoading(false); })).subscribe(res => {
                        Notification.Success('Motivo de rechazo Actualizado.');
                        Helpers.setLoading(false);
                        this.popupDataVisible = false;
                        this.clear_model();
                        this.get_rejectionTypes();
                    },
                        error => {
                            Notification.WebMethodError(error);
                            Helpers.setLoading(false);
                        });

                }

            }
        }

    }

    //Abre ell popup y bloquea el boton de activo ya que los nuevos motivos deben de ir por defecto activos
    new_rejectionType() {
        this.popupDataVisible = true;
        //this.activeDisabled = true;
    }

    //Se le asigna al boton cancelar esta funci�n para que se encargue de limpiar el popup
    cancel_save_rejectionType() {
        this.popupDataVisible = false;
        this.activeDisabled = false;
        this.clear_model();
        this.model.active = true;
        Helpers.setLoading(false);
        this.get_rejectionTypes();
    }

    //Limpia los campos de el modelo
    clear_model() {
        this.model.id = 0;
        this.model.idCompany = 0;
        this.model.rejectionName = '';
    }


    edit_rejectionType_init(e) {

        this.model = e;
        this.popupDataVisible = true;
    }

}
