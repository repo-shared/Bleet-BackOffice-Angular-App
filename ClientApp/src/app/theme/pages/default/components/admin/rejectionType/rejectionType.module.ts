
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { Routes, RouterModule } from '@angular/router';
import { RejectionTypeComponent } from "./rejectionType.component";
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../../../../../../../environments/environment';
import { DxScrollViewModule, DxDataGridModule, DxSwitchModule, DxPopupModule, DxCheckBoxModule, DxTextBoxModule, DxLookupModule, DxDateBoxModule, DxValidatorModule, DxLoadPanelModule } from 'devextreme-angular';
import { RejectionTypeService } from '../../../../../../_services/rejectionType.services';
const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": RejectionTypeComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        DxSwitchModule,
        DxPopupModule,
        DxLookupModule,
        DxDateBoxModule,
        DxTextBoxModule,
        DxLoadPanelModule,
        DxValidatorModule,
        DxCheckBoxModule,
        DxDataGridModule,
        DxScrollViewModule,
        FormsModule
    ], exports: [
        RouterModule
    ], declarations: [
        RejectionTypeComponent
    ],
    providers: [RejectionTypeService, DatePipe],
})
export class RejectionTypeModule {



}