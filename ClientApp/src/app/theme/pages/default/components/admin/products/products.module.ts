
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from "./products.component";
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../../../../../../../environments/environment';
import { DxNumberBoxModule, DxProgressBarModule, DxScrollViewModule, DxDataGridModule, DxSwitchModule, DxPopupModule, DxCheckBoxModule, DxTextBoxModule, DxLookupModule, DxDateBoxModule, DxValidatorModule, DxLoadPanelModule, DxSelectBoxComponent, DxSelectBoxModule, DxFileUploaderModule } from 'devextreme-angular';
import { ProductService } from '../../../../../../_services/product.services';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ProductsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        DxSwitchModule,
        DxPopupModule,
        DxLookupModule,
        DxDateBoxModule,
        DxSelectBoxModule,
        DxTextBoxModule,
        DxLoadPanelModule,
        DxValidatorModule,
        DxCheckBoxModule,
        DxDataGridModule,
        DxScrollViewModule,
        DxFileUploaderModule,
        DxProgressBarModule,
        DxNumberBoxModule,
        FormsModule
    ], exports: [
        RouterModule
    ], declarations: [
        ProductsComponent
    ],
    providers: [ProductService, DatePipe],
})
export class ProductsModule {
}
