
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from "./users.component";
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../../../../../../../environments/environment';
import { DxScrollViewModule, DxDataGridModule, DxSwitchModule, DxPopupModule, DxCheckBoxModule, DxTextBoxModule, DxLookupModule, DxDateBoxModule, DxValidatorModule, DxLoadPanelModule, DxSelectBoxModule } from 'devextreme-angular';



const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": UsersComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        DxSwitchModule,
        DxPopupModule,
        DxLookupModule,
        DxDateBoxModule,
        DxTextBoxModule,
        DxLoadPanelModule,
        DxValidatorModule,
        DxCheckBoxModule,
        DxDataGridModule,
        FormsModule,
        DxScrollViewModule,
        DxSelectBoxModule
    ], exports: [
        RouterModule
    ], declarations: [
        UsersComponent
    ],
    providers: [AngularFirestore, DatePipe],
})
export class UsersModule {



}