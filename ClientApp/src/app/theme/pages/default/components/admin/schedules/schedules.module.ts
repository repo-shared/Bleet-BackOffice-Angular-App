import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { SchedulesComponent } from './schedules.component';
import { SchedulesService } from '../../../../../../_services/schedule.services';
import { WorkDayHourService } from '../../../../../../_services/workdayhour.services';
import { WorkScheduleUserService } from '../../../../../../_services/workscheduleuser.services';
import { DxScrollViewModule, DxDataGridModule, DxSwitchModule, DxPopupModule, DxCheckBoxModule, DxTextBoxModule, DxLookupModule, DxDateBoxModule, DxValidatorModule, DxLoadPanelModule, DxSelectBoxModule } from 'devextreme-angular';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': SchedulesComponent,
            },
        ],
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, DxSwitchModule,
        DxPopupModule,
        DxLookupModule,
        DxDateBoxModule,
        DxTextBoxModule,
        DxLoadPanelModule,
        DxValidatorModule,
        DxCheckBoxModule,
        DxDataGridModule,
        DxScrollViewModule,
        DxSelectBoxModule
    ], exports: [
        RouterModule,
    ], declarations: [
        SchedulesComponent,
    ], providers: [SchedulesService, WorkDayHourService, WorkScheduleUserService]
})
export class SchedulesModule {
}