import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { ProductsComponentHelper } from './products.component.helper';
import { ActivatedRoute, Router } from '@angular/router';
import { PackageService } from '../../../../../../_services/package.services';
import { ProductService } from '../../../../../../_services/product.services';
import { DxDataGridComponent } from 'devextreme-angular';
import { finalize } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

@Component({
    selector: "app-admin-products",
    templateUrl: "./products.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, AfterViewInit {

    public popupDataVisible: boolean = false;

    
    public isDropZoneActive = false;
    public progressVisible = false;
    public progressValue = 0;
    public imageSource = "";
    public versionLocalImage = new Date() 
    @ViewChild(DxDataGridComponent, { static: true }) gridContainer: DxDataGridComponent;

    public model = {
        Id: 0,
        Name: '',
        Description: '',
        Type: '',
        Imagen: '',
        Price: '',
        Active: true
    };

    public typeList = [{
        name: "Litro"
    }, {
        name: "1/2 Litro"
    }, {
        name: "350 ml"
    },
    {
        name: "Galon"
    }];

    public userData;
    public userList;
    public data;
    public loadingVisible;
    applyChanges = false;

    constructor(private _script: ScriptLoaderService,
        private _changeDetector: ChangeDetectorRef,
        private route: ActivatedRoute, private router: Router,
        private prodService: ProductService) {
        //this.userHelper = new PackagesComponentHelper(pkgsSrv);

        this.onDropZoneEnter = this.onDropZoneEnter.bind(this);
        this.onDropZoneLeave = this.onDropZoneLeave.bind(this);
        this.onValueChanged = this.onValueChanged.bind(this);
        this.onProgress = this.onProgress.bind(this);
        this.onUploadStarted = this.onUploadStarted.bind(this);
    }

    ngOnInit() {
        this.getElements();
    }

    ngAfterViewInit() {

    }

    getElements() {
        Helpers.setLoading(true, 'Cargando Productos...');
        this.prodService.getAllProducts().subscribe(res => {
            this.versionLocalImage = new Date() 
            this.data = res;
            Helpers.setLoading(false);
        });
    }

    save() {
        Helpers.setLoading(true, 'Guardando el Producto...');
        this.loadingVisible = true;
        this.model.Imagen = this.imageSource;
        this.prodService.add(this.model).subscribe(res => {
            Helpers.setLoading(false);

            this.getElements();
            this.loadingVisible = false;
            this.popupDataVisible = false;
        });
    }

    edit_init(e) {
        this.popupDataVisible = true;

        this.imageSource = e.imagen;

        const dropZoneText = document.getElementById("dropzone-text");

        if (this.imageSource.startsWith("http")) {
            //dropZoneText.style.display = "none";
        } else {
            //dropZoneText.style.display = "block";
        }

        this.model.Id = e.id;
        this.model.Name = e.name;
        this.model.Description = e.description;
        this.model.Type = e.type;
        this.model.Imagen = e.imagen + "?v=" + this.versionLocalImage;
        this.model.Price = e.price;
        this.model.Active = e.active;
        

    }

    cleanModel() {
        this.model.Id = 0;
        this.model.Name = "";
        this.model.Description = "";
        this.model.Type = "";
        this.model.Imagen = "";
        this.model.Active = true;
        this.model.Price = "";
    }

    onDropZoneEnter(e) {
        if (e.dropZoneElement.id === "dropzone-external")
            this.isDropZoneActive = true;
    }

    onDropZoneLeave(e) {
        if (e.dropZoneElement.id === "dropzone-external")
            this.isDropZoneActive = false;
    }

    onValueChanged(e) {
        console.log(e);

        const file = e.value[0];
        const dropZoneText = document.getElementById("dropzone-text");
        const fileReader = new FileReader();
        fileReader.onload = () => {
            this.isDropZoneActive = false;
            this.imageSource = fileReader.result as string;
        }
        fileReader.readAsDataURL(file);
        dropZoneText.style.display = "none";
        this.progressVisible = false;
        this.progressValue = 0;
    }

    onProgress(e) {
        this.progressValue = e.bytesLoaded / e.bytesTotal * 100;
    }

    onUploadStarted() {
        this.imageSource = "";
        this.progressVisible = true;
    }

}