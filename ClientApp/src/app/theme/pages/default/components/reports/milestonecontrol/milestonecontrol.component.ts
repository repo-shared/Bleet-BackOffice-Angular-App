import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { SubsidiaryService } from '../../../../../../_services/subsidiary.service';
import { Notification } from '../../../../../../utils/notification';
import { Helpers } from '../../../../../../helpers';
import { LocalStorageUtil } from '../../../../../../utils/localstorageutil';
import { UserService } from '../../../../../../_services/user.services';
import { ReportService } from '../../../../../../_services/report.service';
import { PerfilUser } from '../../../../../../utils/perfil.user.enum';

import { ExportAsService, ExportAsConfig } from 'ngx-export-as';

@Component({
    selector: "app-report-milestonecontrol",
    templateUrl: "./milestonecontrol.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./milestonecontrol.component.css']
})

export class MilestoneControlComponent implements OnInit, AfterViewInit {

    public data: any;
    public subSidiaryList: any;
    public userList: any;
    public now: Date = new Date();
    public userLoginData = LocalStorageUtil.getUserLoginData();

    public exportVisible = false;
    
    private exportXlsAsConfig: ExportAsConfig = { type: 'xlsx', elementIdOrContent: 'reportDocument' } 
    private exportCsvAsConfig: ExportAsConfig  = { type: 'csv', elementIdOrContent: 'reportDocument' }

    constructor(private reportService: ReportService, private subsidiaryService: SubsidiaryService, private userService: UserService, private exportAsService: ExportAsService) {
    }

    public filters: any = {
        date: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()),
        idUser: [],
        idSubsidiary: '0',
        milestone: 0
    };

    public milestoneList = [{
        id: 0, name: 'Todos'
    }, {
        id: 1, name: 'Hito 1'
    }, {
        id: 2, name: 'Hito 2'
    }, {
        id: 3, name: 'Hito 3'
    }];

    ngOnInit() {

        this.data = new Array();

        this.subsidiaryService.getSubsidiaries().subscribe(
            result => {
                this.subSidiaryList = result.data;

                this.subSidiaryList.unshift({ id: 0, name: 'Todas' });

                if (this.userLoginData.rol == PerfilUser.Coordinator) {

                    var item = this.subSidiaryList.filter(s => s.name == this.userLoginData.subsidiaryName)[0];

                    if (item)
                        this.filters.idSubsidiary = item.id;


                }

            },
            error => {
                Notification.WebMethodError(error);
            });
    }

    subsidiary_onValueChanged(e) {
        console.log(e.value);

        Helpers.setLoading(true, 'Cargando Usuarios...');
        var method: any;
        if (e.value == 0) {
            method = this.userService.getAll();
        } else {
            method = this.userService.getAllBySubsidiary(e.value);
        }

        method.subscribe(
            result => {
                this.filters.idUser = [];
                var data = new Array();
                result.data.forEach(function(item) {
                    data.push({ id: item.id, fullName: item.fullName, selected: false });
                });

                this.userList = data;

                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });
    }

    exportExcel() {
        this.exportAsService.save(this.exportXlsAsConfig, 'ControlHitos').subscribe(() => {
            this.exportVisible = false;
        });
    }

    exportCsv() {
        this.exportAsService.save(this.exportCsvAsConfig, 'ControlHitos').subscribe(() => {
            this.exportVisible = false;
        });
    }

    generateReport() {
        Helpers.setLoading(true, 'Cargando Hitos...');
        this.reportService.getMilestone(this.filters.date, this.filters.date, '0', '0').subscribe(
            result => {
                this.data = result.data.filter(b => this.filters.idUser.includes(b.userFullName));

                if (this.filters.milestone == 3) {
                    this.data = this.data.filter(b => b.date3 != null);
                }

                if (this.filters.milestone == 2) {
                    this.data = this.data.filter(b => b.date2 != null);
                }

                if (this.filters.milestone == 1) {
                    this.data = this.data.filter(b => b.date1 != null);
                }

                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });

    }

    ngAfterViewInit() {

    }

}