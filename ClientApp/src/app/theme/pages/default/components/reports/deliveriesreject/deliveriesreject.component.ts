import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { SubsidiaryService } from '../../../../../../_services/subsidiary.service';
import { Notification } from '../../../../../../utils/notification';
import { Helpers } from '../../../../../../helpers';
import { LocalStorageUtil } from '../../../../../../utils/localstorageutil';
import { UserService } from '../../../../../../_services/user.services';
import { PerfilUser } from '../../../../../../utils/perfil.user.enum';
import { RejectionTypeService } from '../../../../../../_services/rejectionType.services';

import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { PackageService } from '../../../../../../_services/package.services';

@Component({
    selector: "app-report-deliveriesreject",
    templateUrl: "./deliveriesreject.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./deliveriesreject.component.css']
})
export class DeliveriesRejectComponent implements OnInit, AfterViewInit {

    public data: any = [];
    public subSidiaryList: any;
    public rejectionTypeList;
    public userList: any;
    public now: Date = new Date();
    public userLoginData = LocalStorageUtil.getUserLoginData();

    public exportVisible = false;
    
    private exportXlsAsConfig: ExportAsConfig = { type: 'xlsx', elementIdOrContent: 'reportDocument' } 
    private exportCsvAsConfig: ExportAsConfig  = { type: 'csv', elementIdOrContent: 'reportDocument' }
    
    constructor(private packageService: PackageService, private subsidiaryService: SubsidiaryService, private userService: UserService, private rjTpSrv: RejectionTypeService, private exportAsService: ExportAsService) { }

    public filters: any = {
        startDate: new Date(this.now.getFullYear(), this.now.getMonth(), 1),
        endDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()),
        idUser: [],
        idSubsidiary: '0',
        idRejectionType: 0
    };

    ngOnInit() {

        this.subsidiaryService.getSubsidiaries().subscribe(
            result => {
                this.subSidiaryList = result.data;

                this.subSidiaryList.unshift({ id: 0, name: 'Todas' });

                if (this.userLoginData.rol == PerfilUser.Coordinator) {

                    var item = this.subSidiaryList.filter(s => s.name == this.userLoginData.subsidiaryName)[0];

                    if (item)
                        this.filters.idSubsidiary = item.id;


                }

            },
            error => {
                Notification.WebMethodError(error);
            });

        Helpers.setLoading(true, 'Cargando Motivos de Rechazo...');

        return new Promise((resolve) => {

            this.rjTpSrv.getAll().subscribe(res => {
                this.rejectionTypeList = res.data;

                this.rejectionTypeList.sort(function(a, b) { return b.id - a.id; })

                this.rejectionTypeList.unshift({ id: 0, rejectionName: 'Todos' });
                Helpers.setLoading(false);
                resolve();
            },
                error => {
                    Notification.WebMethodError(error);
                    Helpers.setLoading(false);
                });
        });
    }

    subsidiary_onValueChanged(e) {
        console.log(e.value);

        Helpers.setLoading(true, 'Cargando Usuarios...');
        var method: any;
        if (e.value == 0) {
            method = this.userService.getAll();
        } else {
            method = this.userService.getAllBySubsidiary(e.value);
        }

        method.subscribe(
            result => {
                this.filters.idUser = [];
                var data = new Array();
                result.data.forEach(function(item) {
                    data.push({ id: item.id, fullName: item.fullName, selected: false });
                });

                this.userList = data;

                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });
    }

    exportExcel() {
        this.exportAsService.save(this.exportXlsAsConfig, 'ReporteRechazados').subscribe(() => {
            this.exportVisible = false;
        });
    }

    exportCsv() {
        this.exportAsService.save(this.exportCsvAsConfig, 'ReporteRechazados').subscribe(() => {
            this.exportVisible = false;
        });
    }

    generateReport() {
        Helpers.setLoading(true, 'Generando Reporte...');

        this.packageService.getFinalized("", this.filters.startDate, this.filters.endDate).subscribe(result => {
            this.data = result.data.filter(b => this.filters.idUser.includes(b.idUser) && b.statusId == 5 && (b.rejectionId == this.filters.idRejectionType || 0 == this.filters.idRejectionType));
            Helpers.setLoading(false);
        });
    }

    ngAfterViewInit() {

    }

}