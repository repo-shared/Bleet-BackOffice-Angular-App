import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';

import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { ReportService } from '../../../../../../_services/report.service';
import { SubsidiaryService } from '../../../../../../_services/subsidiary.service';
import { Notification } from '../../../../../../utils/notification';
import { Helpers } from '../../../../../../helpers';
import { LocalStorageUtil } from '../../../../../../utils/localstorageutil';
import { UserService } from '../../../../../../_services/user.services';
import { PerfilUser } from '../../../../../../utils/perfil.user.enum';


@Component({
    selector: "app-report-log",
    templateUrl: "./log.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit, AfterViewInit {

    public data: any;
    public subSidiaryList: any;
    public userList: any;
    public now: Date = new Date();
    public filters: any = {
        startDate: new Date(this.now.getFullYear(), this.now.getMonth(), 1),
        endDate: new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()),
        idUser: [],
        idSubsidiary: '0'
    };

    public userLoginData = LocalStorageUtil.getUserLoginData();

    public exportVisible = false;
    
    private exportXlsAsConfig: ExportAsConfig = { type: 'xlsx', elementIdOrContent: 'reportDocument' } 
    private exportCsvAsConfig: ExportAsConfig  = { type: 'csv', elementIdOrContent: 'reportDocument' }
    
    constructor(private reportService: ReportService, private subsidiaryService: SubsidiaryService, private userService: UserService, private exportAsService: ExportAsService) {

    }

    ngOnInit() {

        this.data = new Array();

        this.subsidiaryService.getSubsidiaries().subscribe(
            result => {
                this.subSidiaryList = result.data;

                this.subSidiaryList.unshift({ id: 0, name: 'Todas' });

                if (this.userLoginData.rol == PerfilUser.Coordinator) {

                    var item = this.subSidiaryList.filter(s => s.name == this.userLoginData.subsidiaryName)[0];

                    if (item)
                        this.filters.idSubsidiary = item.id;


                }

            },
            error => {
                Notification.WebMethodError(error);
            });
    }

    subsidiary_onValueChanged(e) {
        console.log(e.value);

        Helpers.setLoading(true, 'Cargando Usuarios...');
        var method: any;
        if (e.value == 0) {
            method = this.userService.getAll();
        } else {
            method = this.userService.getAllBySubsidiary(e.value);
        }

        method.subscribe(
            result => {
                this.filters.idUser = [];
                var data = new Array();
                result.data.forEach(function(item) {
                    data.push({ id: item.id, fullName: item.fullName, selected: false });
                });

                this.userList = data;

                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });
    }

    exportExcel() {
        this.exportAsService.save(this.exportXlsAsConfig, 'Logs').subscribe(() => {
            this.exportVisible = false;
        });
    }

    exportCsv() {
        this.exportAsService.save(this.exportCsvAsConfig, 'Logs').subscribe(() => {
            this.exportVisible = false;
        });
    }

    generateReport() {
        Helpers.setLoading(true, 'Cargando Hitos...');
        this.reportService.getActivityLogs(this.filters.startDate, this.filters.endDate, '0', '0').subscribe(
            result => {
                this.data = result.data.filter(b => this.filters.idUser.includes(b.userFullName));
                Helpers.setLoading(false);
            },
            error => {
                Notification.WebMethodError(error);
                Helpers.setLoading(false);
            });

    }

    ngAfterViewInit() {

    }

}