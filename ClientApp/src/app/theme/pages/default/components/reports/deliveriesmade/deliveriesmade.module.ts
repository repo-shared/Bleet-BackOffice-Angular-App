
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { Routes, RouterModule } from '@angular/router';
import { DeliveriesMadeComponent } from "./deliveriesmade.component";
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { DxTagBoxModule, DxDataGridModule, DxSwitchModule, DxPopupModule, DxCheckBoxModule, DxTextBoxModule, DxLookupModule, DxDateBoxModule, DxValidatorModule, DxLoadPanelModule, DxSelectBoxComponent, DxSelectBoxModule } from 'devextreme-angular';
import { PackageService } from '../../../../../../_services/package.services';
import { SubsidiaryService } from '../../../../../../_services/subsidiary.service'
import { UserService } from '../../../../../../_services/user.services'

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": DeliveriesMadeComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,
        DxSwitchModule,
        DxPopupModule,
        DxLookupModule,
        DxDateBoxModule,
        DxSelectBoxModule,
        DxTextBoxModule,
        DxLoadPanelModule,
        DxValidatorModule,
        DxCheckBoxModule,
        DxDataGridModule,
        DxTagBoxModule,
        FormsModule
    ], exports: [
        RouterModule
    ], declarations: [
        DeliveriesMadeComponent
    ],
    providers: [PackageService, SubsidiaryService, UserService, DatePipe],
})
export class DeliveriesMadeModule {
}
