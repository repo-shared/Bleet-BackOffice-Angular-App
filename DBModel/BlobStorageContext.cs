using Microsoft.Azure.Cosmos;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Net;
using Azure.Storage.Blobs;
using System.IO;

namespace Bleet_Web.DBModel
{

    public class BlobStorageContext
    {
        private static readonly string connectionString = "DefaultEndpointsProtocol=https;AccountName=bleetstorage;AccountKey=wgOWHQBuLNBEE0uOxbxwBo5pCAVTjMA77SZIlXzx6Au0bB0Ky83hGEtioaCI/A8Zz6NMf+pVroySxxaVUGqARw==;EndpointSuffix=core.windows.net";
        private static readonly string PrimaryKey = "wgOWHQBuLNBEE0uOxbxwBo5pCAVTjMA77SZIlXzx6Au0bB0Ky83hGEtioaCI/A8Zz6NMf+pVroySxxaVUGqARw==";
        private static readonly string containerNameImages = "bleet-images";


        public static async Task<bool> UploadImageFile(string fileName, string fileBase64)
        {
            try
            {

                fileBase64 = fileBase64.Replace("data:image/png;base64,", "");

                BlobServiceClient blobServiceClient = new BlobServiceClient(connectionString);
                BlobContainerClient containerClient ;
                
                try{
                    containerClient = await blobServiceClient.CreateBlobContainerAsync(containerNameImages);
                }catch(Exception ex){
                    containerClient =  blobServiceClient.GetBlobContainerClient(containerNameImages);
                }

                BlobClient blobClient = containerClient.GetBlobClient(fileName + ".png");

                byte[] newBytes = Convert.FromBase64String(fileBase64);

                using MemoryStream uploadFileStream = new MemoryStream(newBytes);
                var resp = await blobClient.UploadAsync(uploadFileStream, true);
                
                uploadFileStream.Close();

                return true;

            }
            catch (Exception ex)
            {
                return false;

            }
        }

    }

}