using Newtonsoft.Json;
using System.Collections.Generic;
using System;

namespace Bleet_Web.DBModel
{

     public class Orders
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public Client Client { get; set; }
        public List<Products> Products { get; set; }
        public Status Status { get; set; }
        
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }


    public enum Status{
        Order,
        InProgress,
        Delivered,
        Rejected    
    }

}