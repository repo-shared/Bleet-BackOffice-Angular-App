using Microsoft.Azure.Cosmos;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Net;

namespace Bleet_Web.DBModel
{

    public class CosmosDbContext
    {
        private static readonly string EndpointUri = "https://bleet-cosmosdb.documents.azure.com:443/";
        private static readonly string PrimaryKey = "V4wG6CtfkijDi8rH3zi7nOKotU9UAlgCPZW20VEFyJjTh7M1A3AnRgWRZHiPRKdVYUq4cJIgNW9yocykaGhmtQ==";
        private static readonly string databaseId = "bleet";


        public static async Task<List<T>> GetData<T>(string containerId, string sqlQueryText)
        {
            try
            {
                QueryDefinition queryDefinition = new QueryDefinition(sqlQueryText);

                CosmosClient cosmosClient = new CosmosClient(EndpointUri, PrimaryKey);
                Database database = await cosmosClient.CreateDatabaseIfNotExistsAsync(databaseId);
                Container container = await database.CreateContainerIfNotExistsAsync(containerId, "/id");

                FeedIterator<T> queryResultSetIterator = container.GetItemQueryIterator<T>(queryDefinition);

                List<T> info = new List<T>();

                while (queryResultSetIterator.HasMoreResults)
                {
                    FeedResponse<T> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                    foreach (T item in currentResultSet)
                    {
                        info.Add(item);
                    }
                }

                return info;

            }
            catch (Exception ex)
            {
                return null;

            }
        }
        public static async Task<bool> AddData<T>(string containerId, T objectClass, string id)
        {
            CosmosClient cosmosClient = new CosmosClient(EndpointUri, PrimaryKey);
            Database database = await cosmosClient.CreateDatabaseIfNotExistsAsync(databaseId);
            Container container = await database.CreateContainerIfNotExistsAsync(containerId, "/id");

            try
            {
                ItemResponse<T> itemResponse = await container.ReadItemAsync<T>(id, new PartitionKey(id));
                
                ItemResponse<T> resp = await container.ReplaceItemAsync<T>(objectClass, id, new PartitionKey(id));

            }
            catch (CosmosException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
            {
                // Create an item in the container representing the Andersen family. Note we provide the value of the partition key for this item, which is "Andersen"
                ItemResponse<T> itemResponse = await container.CreateItemAsync<T>(objectClass, new PartitionKey(id));
                
            }

            return true;
        }



    }

}