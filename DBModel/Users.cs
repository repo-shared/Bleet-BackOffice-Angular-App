using Newtonsoft.Json;

namespace Bleet_Web.DBModel
{

     public class Users
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        public string User { get; set; }
        public string Pass { get; set; }
        
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

}