using Newtonsoft.Json;

namespace Bleet_Web.DBModel
{

     public class Products
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Imagen { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public bool Active { get; set; }
        
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

}