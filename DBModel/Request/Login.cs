using Newtonsoft.Json;

namespace Bleet_Web.DBModel.Request
{

     public class Login
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

}